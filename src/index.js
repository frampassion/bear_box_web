import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux'
import promiseMiddleware from 'redux-promise-middleware';
import { createStore, applyMiddleware } from 'redux';
import createHistory from 'history/createBrowserHistory';

import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import rootReducer from './Reducer/root';

import { createLogger } from 'redux-logger'
import { createPromise } from 'redux-promise-middleware'

const history = createHistory();
// const middleware = routerMiddleware(history);


const store = createStore(
    rootReducer,
        applyMiddleware(
            createLogger(),
            // middleware,
            thunk,
            createPromise()
        ) 
);
ReactDOM.render(
<Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
