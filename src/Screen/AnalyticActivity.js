import React, { Component, Fragment } from 'react';
import { reader } from '../Component/ExcelReaderComponent'
import { get } from '../service/service'
import moment from 'moment'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import Calader from '../Component/Calader'
import Fade from 'react-reveal/Fade';

import cash from '../image/business.png'
import credit from '../image/credit-card.png'
import truemoney from '../image/truemoney.jpg'
import xcash from '../image/xcash.jpg'
import promptpay from '../image/promptpay.png'
import alipay from '../image/Alipay.png'
import wechat from '../image/wechat.png'
import ReactLightCalendar from '@lls/react-light-calendar'
import '@lls/react-light-calendar/dist/index.css'
import { connect } from 'react-redux';
import { user_token } from '../Config/constance';
let cash_type = [
    [cash, "CASH"],
    [credit, "CREDITCARD"],
    [truemoney, "TRUEMONEY"],
    [xcash, "XCASH"],
    [promptpay, "PROMPTPAY"],
    [alipay, "ALIPAY"],
    [wechat, "WECHAT"]
]

let tab = [
    "ทั้งหมด",
    "เครื่องดื่ม",
    "ขนม",
    "น้ำอัดลม",
    "ของใช้",
    "ของสด"
]

const product_type = [
    "",
    "Beverage",
    "Processed Food",
    "Non Alcohol",
    "Non Food",
    "ของสด"
]


let scale_color = [
    "FF0000",
    "FF4400",
    "FF6600",
    "FF8800",
    "FFCC00",
    "CCFF00",
    "AAFF00",
    "44FF00",
    "22FF00",
    "00FF00"
]

class AnalyticActivity extends Component {

    constructor() {
        super();
        this.state = {
            activeTab: 'tab1',
            order_data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            sales_data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            order_sum: 0,
            sales_sum: 0,
            payment: [0, 0, 0, 0, 0, 0, 0],
            start_date_report: moment("0000-01-01T00:00:00.000Z"),
            end_date_report: moment("0000-01-01T00:00:00.000Z"),


            startDate: new Date(new Date().getTime()).setDate(new Date().getDate() - 6), // Today + 6 days new Date().getTime(), // Today
            endDate: new Date().getTime(), // Today
            rating: [],
            rating_product_name: [],
            report_information: [],
            vending_information: [],
            total_price_date: [],
            total_price: [],
            filter_product_type: "",
            product_pattle: {
                series: [],
                categories: [],
                product_rank: {
                    categories: [],
                    series: []
                },
                product_rank_merge_frequent: {
                    categories: [],
                    series: []
                },
                frequent: {
                    high: [],
                    mid: [],
                    low: []
                }
            },


        }
    }


    make_data = (raw_data, raw_rating_data) => {
        let order_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        let sales_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        let payment = [0, 0, 0, 0, 0, 0, 0]
        let order_sum = 0
        let sales_sum = 0

        let start_date_report = moment()
        let end_date_report = moment("0000-01-01T00:00:00.000Z")
        let rating = []
        let rating_product_name = []




        raw_rating_data.sort((rating_a, rating_b) => {
            let comparison = 0;
            if (rating_a.rating > rating_b.rating) {
                comparison = 1;
            } else if (rating_a.rating > rating_b.rating) {
                comparison = -1;
            }
            return comparison;
        });


        raw_rating_data.map((raw_rating_data_element) => {
            rating.push(raw_rating_data_element.rating)
            rating_product_name.push(raw_rating_data_element.product_name)
        })







        raw_data.map((raw_data_element) => {

            var format = 'HH:mm:ss'
            let i = moment("07:00:00", format).format("HH:mm:ss")
            let index = 0
            let create_time = moment(raw_data_element.created_on).format("HH:mm:ss")


            do {
                let before_time = moment(i, format)
                let after_time = moment(i, format).add(119, 'minutes')
                // after_time = after_time.add(1, 'hours')
                // after_time.diff(1, 'minutes')

                // console.log("before_time",before_time)
                // console.log(moment(create_time, format).format("HH:mm:ss"),before_time.format(format),after_time.format(format))
                // console.log("test",moment('17:10:54', format).isBetween(before_time, after_time))
                // console.log(before_time.format(format),moment(create_time, format).format("HH:mm:ss"),after_time.format(format))
                // console.log("condition",moment(create_time, format).isBetween(before_time, after_time) ,index)


                if (moment(create_time, format).isBetween(before_time, after_time)) {

                    order_data[index] += 1  //count
                    sales_data[index] += raw_data_element.last_price //sum
                }
                i = moment(i, "HH:mm:ss").add(2, 'hours').format("HH:mm:ss")
                index += 1
            } while (i != moment("07:00:00", "HH:mm:ss").format("HH:mm:ss"))


            cash_type.map((cash_type_element, cash_type_index) => {
                if (raw_data_element.payment === cash_type_element[1]) {

                    payment[cash_type_index] += raw_data_element.last_price //sum

                    // console.log(payment)
                }
            })


            if (moment(raw_data_element.created_on).isBefore(start_date_report)) {
                start_date_report = moment(raw_data_element.created_on)
            } else if (moment(raw_data_element.created_on).isAfter(end_date_report)) {
                end_date_report = moment(raw_data_element.created_on)
            }

            // console.log( moment(raw_data_element.created_on).isAfter(end_date_report) )








            order_sum += 1  //count_all
            sales_sum += raw_data_element.last_price //sum_value_all

        })


        this.setState({
            order_data: order_data,
            sales_data: sales_data,
            sales_sum: sales_sum,
            cash_type: cash_type,
            start_date_report: start_date_report,
            end_date_report: end_date_report,
            rating: rating,
            payment: payment,
            rating_product_name: rating_product_name
        })



    }






    get_pattle_report = async (branch_code, start_date, end_date) => {

        let startDate = moment(start_date).format("YYYY-MM-DD")
        let endDate = moment(end_date).format("YYYY-MM-DD")

        try {
            await get(`report/pattern_order/${branch_code}/${startDate}/${endDate}`, null).then((res) => {
                if (res.success) {


                    this.setState({
                        product_pattle: res.result,
                    })

                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }

    report_sell_conclude = async (branch_code, start_date, end_date) => {


        let startDate = moment(start_date).format("YYYY-MM-DD")
        let endDate = moment(end_date).format("YYYY-MM-DD")

        try {
            await get(`report/report_sell_conclude/${branch_code}/${startDate}/${endDate}`, null).then((res) => {
                if (res.success) {
                    let total_price = []
                    let total_price_date = []


                    console.log(res.result)

                    res.result.reverse().map((result_element) => {
                        total_price.push(result_element.total_price)
                        total_price_date.push(moment(result_element.date_end).format("DD/MM/YYYY"))

                    })

                    // this.get_pattle_report(branch_code)

                    this.setState({
                        total_price: total_price,
                        total_price_date: total_price_date
                    })






                    // this.setState({
                    //     product_pattle: res.result,
                    // })

                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }



    // get_report_information = async (branch_code) => {
    //     try {
    //         await get(`report/report_information/${branch_code}`, null).then((res) => {
    //             if (res.success) {

    //                 let total_price = []
    //                 let total_price_date = []


    //                 res.result.reverse().map((result_element) => {
    //                     total_price.push(result_element.total_price)
    //                     total_price_date.push(moment(result_element.date_end).format("DD/MM/YYYY"))

    //                 })

    //                 this.get_pattle_report(branch_code)

    //                 this.setState({
    //                     report_information: res.result.reverse(),
    //                     total_price: total_price,
    //                     total_price_date: total_price_date
    //                 })

    //                 // if (res.result[0]) {
    //                 //     this.get_order(res.result[0].id)
    //                 // }

    //             } else {
    //                 console.log("ERROR !!! :", res.message)
    //             }
    //         }).catch((err) => {
    //             console.log(err)
    //         })
    //     } catch (err) {
    //         console.log(err)
    //     }
    // }

    get_vending_information = async () => {
        try {
            await get('report/vending_information/', user_token).then((res) => {

                if (res.success) {
                  
                    this.get_pattle_report(res.result[0].vending_branch_code, this.state.startDate, this.state.endDate)
                    this.report_sell_conclude(res.result[0].vending_branch_code, this.state.startDate, this.state.endDate)
                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }

    componentDidMount = () => {
        if (!this.props.VendingList.vending_select_value) {
            this.get_vending_information()
            
        } else {
            this.get_pattle_report(this.props.VendingList.vending_select_value, this.state.startDate, this.state.endDate)
            this.report_sell_conclude(this.props.VendingList.vending_select_value, this.state.startDate, this.state.endDate)
        }
    }

    onChangeDate = (startDate, endDate) => {
        // console.log("startDate",moment(startDate).format('DD-MM-YYYY'))
        // console.log("endDate",moment(endDate).format('DD-MM-YYYY'))
        this.setState({ startDate, endDate })
        if (startDate && endDate) {
            if (startDate > endDate) {
                this.setState({ startDate: endDate, endDate: startDate })

                this.get_pattle_report(this.props.VendingList.vending_select_value, endDate, startDate)
                this.report_sell_conclude(this.props.VendingList.vending_select_value, endDate, startDate)
            } else {


                this.get_pattle_report(this.props.VendingList.vending_select_value, startDate, endDate)
                this.report_sell_conclude(this.props.VendingList.vending_select_value, startDate, endDate)
            }
        }
    }


    OnchangeBranch = (branch_code) => {

        this.get_pattle_report(branch_code, this.state.startDate, this.state.endDate)
        this.report_sell_conclude(branch_code, this.state.startDate, this.state.endDate)


    }

    btn_m = (month, index) => {

        // console.log("month", index);
        if (month == "01" || month == "05" || month == "09") {
            if (moment(this.state.end_date_report).format("DD/MM/YYYY") == index) {
                return month = "active1";
            }
            return month = "btn-date1";
        }
        if (month == "02" || month == "06" || month == "10") {
            if (moment(this.state.end_date_report).format("DD/MM/YYYY") == index) {
                return month = "active2";
            }
            return month = "btn-date2";
        }
        if (month == "03" || month == "07" || month == "11") {
            if (moment(this.state.end_date_report).format("DD/MM/YYYY") == index) {
                return month = "active3";
            }
            return month = "btn-date3";
        }
        if (month == "04" || month == "08" || month == "12") {
            if (moment(this.state.end_date_report).format("DD/MM/YYYY") == index) {
                return month = "active4";
            }
            return month = "btn-date4";
        }
    }

    No_graph_render = (data) => {
        let data_series = []



        data.map((series_el, series_index) => {
            data_series.push(`${series_el}  อันดับที่  ${series_index + 1} `)
        })
        return data_series
    }



    product_pattle_filter = () => {



        
        let line_graph_array = this.state.product_pattle.series.filter((product_pattle) => {
            return product_pattle.product_type ? product_pattle.product_type.indexOf(this.state.filter_product_type) > -1 : false
        });
        {console.log("test",line_graph_array)}
        return (line_graph_array)


        // this.state.product_pattle.series.map((product_pattle) => {
        //         console.log(product_pattle.product_type.indexOf(product_type) > -1) 
        //       });

        // return product_pattle.product_type.indexOf(product_type) > -1;



    }



    render = () => {

        let options = {
            // chart: {
            //     zoomType: 'xy'
            // },
            title: {
                text: 'ยอดขาย'
            },
            subtitle: {
                text: `วันที่  ${this.state.start_date_report.format("DD/MM/YYYY")} - ${this.state.end_date_report.format("DD/MM/YYYY")}`
            },
            xAxis: [{
                categories: ['7 - 9 น.', '9 - 11 น.', '11 - 13 น.', '13 - 15 น.', '15 - 17 น.', '17 - 19 น.',
                    '19 - 21 น.', '21 - 23 น.', '23 - 01 น.', '01 - 03 น.', '03 - 05 น.', '05 - 07 น.'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'จำนวนออเดอร์ (ชิ้น)',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'ยอดขาย (บาท)',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                // layout: 'vertical',
                // align: 'right',
                // x: 120,
                // verticalAlign: 'top',
                // y: 100,
                // floating: true,


                // layout: 'vertical',
                // align: 'right',
                // verticalAlign: 'middle',

                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || // theme
                    'rgba(255,255,255,0.25)'
            },
            series: [{
                name: 'ยอดขาย',
                type: 'column',
                yAxis: 1,
                data: this.state.sales_data,
                tooltip: {
                    valueSuffix: ' บาท'
                }
            }, {
                name: 'จำนวนออเดอร์',
                type: 'spline',
                data: this.state.order_data,
                tooltip: {
                    valueSuffix: ' ออเดอร์'
                }
            }]
        };


        let rating_options = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'สินค้าขายดี'
            },

            xAxis: {
                categories: this.state.rating_product_name,
                crosshair: true
            },
            yAxis: { // Primary yAxis
                labels: {
                    format: '{value} product_นออเดอร์',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'จำนวนออเดอร์',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            },
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || // theme
                    'rgba(255,255,255,0.25)'
            },
            series: {
                name: 'จำนวนออเดอร์',
                type: 'spline',
                data: this.state.rating,
                tooltip: {
                    valueSuffix: 'ออเดอร์'
                }
            }
        };

        let categoryHeight = 40;

        let total_rating_options = {


            chart: {
                type: 'bar',
                // events: {
                //     load: function () {
                //         let categoryHeight = 30;

                //         alert(JSON.stringify(this.state))
                //         // this.update({
                //         //     chart: {
                //         //         height: this.state.product_pattle.product_rank.categories != [] ? categoryHeight * this.state.product_pattle.product_rank.categories.length : 0
                //         //     }
                //         // })
                //     }
                // }

                height: this.state.product_pattle.product_rank.categories != [] ? categoryHeight * this.state.product_pattle.product_rank.categories.length : 0

            },

            title: {
                text: 'จำนวนออเดอร์'
            },

            xAxis: {
                categories: this.No_graph_render(this.state.product_pattle.product_rank.categories),
                crosshair: true
            },
            yAxis: { // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'จำนวนออเดอร์ (ชิ้น)',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                shared: true
            },

            legend: {
                layout: 'vertical',
                align: 'right',
                x: 0,
                verticalAlign: 'top',
                y: 0,
                floating: true,
            },

            series: [{
                name: 'จำนวนออเดอร์',
                type: 'column',
                // yAxis: 1,
                data: this.state.product_pattle.product_rank.series,
                tooltip: {
                    valueSuffix: ' ชิ้น'
                },
                dataLabels: {
                    enabled: true,
                    align: 'right',
                    format: '{point.y}', // one decimal
                    x: 20, // 10 pixels down from the top
                }
            }]
        };


        let total_price_options = {
            chart: {
                type: 'line'
            },
            title: {
                text: 'ยอดขายแต่ละวัน'
            },
            xAxis: {
                categories: this.state.total_price_date,
                crosshair: true
            },
            yAxis: { // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'ยอดขาย (บาท)',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            },
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                x: 0,
                verticalAlign: 'top',
                y: 0,
                floating: true,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || // theme
                    'rgba(255,255,255,0.25)'
            },
            series: [{
                name: 'ยอดขาย',
                data: this.state.total_price,
                type: 'spline',
                tooltip: {
                    valueSuffix: ' บาท'
                }
                // pointStart: 1
            }]


            // title: {
            //     text: 'Logarithmic axis demo'
            // },

            // xAxis: {
            //     categories: this.state.total_price_date,
            //     crosshair: true
            // },

            // yAxis: { // Primary yAxis
            //     labels: {
            //         format: '{value}',
            //         style: {
            //             color: Highcharts.getOptions().colors[1]
            //         }
            //     },
            //     title: {
            //         text: 'ยอดขาย (บาท)',
            //         style: {
            //             color: Highcharts.getOptions().colors[1]
            //         }
            //     }
            // },

            // tooltip: {
            //     headerFormat: '<b>{series.name}</b><br />',
            //     pointFormat: '{point.y} บาท'
            // },

            // series: [{
            //     name: 'ยอดขาย',
            //     data: this.state.total_price,
            //     type: 'spline',
            //     // pointStart: 1
            // }]

        };



        let pattle_product_option = {
            chart: {
                type: 'line'
            },
            title: {
                text: 'จำนวนการขายสินค้าในแต่ละวัน'
            },
            subtitle: {
                text: ' '
            },
            xAxis: {
                categories: this.state.product_pattle.categories
            },
            yAxis: {
                title: {
                    text: 'ยอดขาย (ชิ้น)'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: this.product_pattle_filter()
        }




        let rating_merge_frequent_option = {
            // chart: {
            //     type: 'column'
            // },
            // title: {
            //     text: 'จำนวนออเดอร์ + ความถี่การซื้อ weight 80:20'
            // },

            // xAxis: {
            //     categories: this.state.product_pattle.product_rank_merge_frequent.categories,
            //     crosshair: true
            // },
            // yAxis: { // Primary yAxis
            //     labels: {
            //         format: '{value} %',
            //         style: {
            //             color: Highcharts.getOptions().colors[1]
            //         }
            //     },
            //     title: {
            //         text: 'WIEGHT',
            //         style: {
            //             color: Highcharts.getOptions().colors[1]
            //         }
            //     }
            // },
            // tooltip: {
            //     shared: true
            // },
            // legend: {
            //     layout: 'vertical',
            //     align: 'left',
            //     x: 120,
            //     verticalAlign: 'top',
            //     y: 100,
            //     floating: true,
            //     backgroundColor:
            //         Highcharts.defaultOptions.legend.backgroundColor || // theme
            //         'rgba(255,255,255,0.25)'
            // },
            // series: {
            //     name: 'จำนวนออเดอร์',
            //     type: 'spline',
            //     data: this.state.product_pattle.product_rank_merge_frequent.series,
            //     tooltip: {
            //         valueSuffix: 'ออเดอร์'
            //     }
            // }


            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'จำนวนออเดอร์ + ความถี่การซื้อ weight 80:20'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.z:.1f} %</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.z:.1f} %'
                    }
                }
            },
            series: [{
                name: 'จำนวน',
                colorByPoint: true,
                data: [{
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[0],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[0],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[0],
                },
                {
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[1],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[1],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[1],
                }, {
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[2],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[2],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[2],
                }, {
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[3],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[3],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[3],
                }, {
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[4],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[4],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[4],
                }, {
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[5],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[5],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[5],
                }, {
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[6],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[6],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[6],
                }, {
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[7],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[7],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[7],
                }, {
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[8],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[8],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[8],
                }, {
                    name: this.state.product_pattle.product_rank_merge_frequent.categories[9],
                    y: this.state.product_pattle.product_rank_merge_frequent.series[9],
                    z: this.state.product_pattle.product_rank_merge_frequent.series[9],
                }]
            }]


        };

        return (
            <div>
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10 " >
                        <div className="bg-report" style={{position: "fixed"}}>
                            <div className="fs-52 center-text animate-top" style={{ color: "#FFF", paddingTop: "15px" }}>Report</div>
                            <select className="box" value={this.props.VendingList.vending_select_value} onChange={(e) => {
                                this.props.setVendingSelectValue(e.target.value)
                                this.OnchangeBranch(e.target.value)
                            }}>
                                {
                                    this.props.VendingList.vending_list.map((vending_information_element) => {
                                        return (
                                            <option value={vending_information_element.vending_branch_code}>{vending_information_element.vending_branch_name}</option>
                                        )
                                    })
                                }

                            </select>
                        </div>
                        <br /><br /><br /><br />
                        <div className="row" style={{marginTop:"120px"}}>
                            <div className="col-3">
                                <div style={{ marginLeft: "-5px", position: "fixed" }}><ReactLightCalendar startDate={this.state.startDate} endDate={this.state.endDate} onChange={this.onChangeDate} range disableDates={date => date >= new Date().getTime()} />
                                </div>
                            </div>
                            <div className="col-9">
                                <div>
                                    {tab.map((tabName, tabIndex) => {
                                        return (
                                            <button type="button"
                                                onClick={(e) => {
                                                    e.preventDefault()

                                                    this.setState({ activeTab: `tab${tabIndex + 1}`, filter_product_type: product_type[tabIndex] })
                                                }}
                                                className={this.state.activeTab === `tab${tabIndex + 1}` ? this.state.activeTab === `tab${tabIndex + 1}` && 'activeTab' : 'tab-header'}
                                            >{tabName}</button>
                                        )
                                    })}
                                    {this.state.activeTab === 'tab1' && <div className="tab-area">
                                        <Fragment>
                                            <Fade bottom>
                                                <HighchartsReact highcharts={Highcharts} options={pattle_product_option} />
                                            </Fade>
                                        </Fragment>

                                    </div>}
                                    {this.state.activeTab === 'tab2' && <div className="tab-area">
                                        <Fragment>
                                            <Fade bottom>
                                                <HighchartsReact highcharts={Highcharts} options={pattle_product_option} />
                                            </Fade>
                                        </Fragment>
                                    </div>}
                                    {this.state.activeTab === 'tab3' && <div className="tab-area">

                                        <Fragment>
                                            <Fade bottom>
                                                <HighchartsReact highcharts={Highcharts} options={pattle_product_option} />
                                            </Fade>
                                        </Fragment>
                                    </div>}
                                    {this.state.activeTab === 'tab4' && <div className="tab-area">

                                        <Fragment>
                                            <Fade bottom>
                                                <HighchartsReact highcharts={Highcharts} options={pattle_product_option} />
                                            </Fade>
                                        </Fragment>
                                    </div>}
                                    {this.state.activeTab === 'tab5' && <div className="tab-area">

                                        <Fragment>
                                            <Fade bottom>
                                                <HighchartsReact highcharts={Highcharts} options={pattle_product_option} />
                                            </Fade>
                                        </Fragment>
                                    </div>}
                                    {this.state.activeTab === 'tab6' && <div className="tab-area">

                                        <Fragment>
                                            <Fade bottom>
                                                <HighchartsReact highcharts={Highcharts} options={pattle_product_option} />
                                            </Fade>
                                        </Fragment>
                                    </div>}
                                </div>


                                {/* <Calader get_pattern_data={(startDate,endDate)=>{this.get_pattern_data(startDate,endDate)}} /> */}



                                {/* <Fragment>
                            <Fade bottom>
                                <HighchartsReact highcharts={Highcharts} options={pattle_product_option} />
                            </Fade>
                        </Fragment> */}


                                {/*สินค้าขายดี อยากจะโยกไปหน้า Producr List*/}

                                {/*<div className="row">
                          <div className="col-4"> <div style={{ color: "#ff0000", fontSize: 25 }}> สินค้าขายบ่อยมาก </div>
                                <Fragment>{
                                    this.state.product_pattle.frequent.high.map((high_element) => {
                                        return (
                                            <Fade bottom>
                                                <div>{high_element.name}</div>
                                            </Fade>
                                        )
                                    })
                                }</Fragment>
                            </div>

                            <div className="col-4"> <div style={{ color: "#ff0000", fontSize: 25 }} > สินค้าขายปานกลาง </div>
                                <Fragment>{
                                    this.state.product_pattle.frequent.mid.map((mid_element) => {
                                        return (
                                            <Fade bottom>
                                                <div>{mid_element.name}</div>
                                            </Fade>
                                        )
                                    })
                                }</Fragment>
                            </div>

                            <div className="col-4"> <div style={{ color: "#ff0000", fontSize: 25 }} > สินค้าขายความถี่น้อย </div>
                                <Fragment>{
                                    this.state.product_pattle.frequent.low.map((low_element) => {
                                        return (
                                            <Fade bottom>
                                                <div>{low_element.name}</div>
                                            </Fade>
                                        )
                                    })
                                }</Fragment>
                            </div>
                        </div> */}

                                <br /><br />
                                <hr />
                                <br /><br /><br /><br />


                                {/* <div> สินค้าขายดี รวม  </div> */}

                                <Fragment>
                                    <Fade bottom><HighchartsReact highcharts={Highcharts} options={total_price_options} /></Fade>
                                </Fragment>
                                <br /><br />
                                <hr />
                                <br /><br /><br /><br />
                                <Fragment>
                                    <Fade bottom><HighchartsReact highcharts={Highcharts} options={rating_merge_frequent_option} /></Fade>
                                </Fragment>
                                <br /><br />
                                <hr />
                                <br /><br /><br /><br />
                                <Fragment>
                                    <Fade bottom><HighchartsReact highcharts={Highcharts} options={total_rating_options} /></Fade>
                                </Fragment>

                                <br /><br />
                                <hr />
                                <br /><br /><br /><br />



                            </div>
                        </div>

                    </div>

                </div>
            </div>
        )
    }




}

const mapStateToProps = (state) => {
    return {
        VendingList: state.VendingList,
    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        setVendingList: (data) => {
            dispatch(
                {
                    type: "SET_VENDING_LIST",
                    payload: data
                }
            )
        },
        setVendingSelectValue: (data) => {
            dispatch(
                {
                    type: "SET_VENDING_SELECT_VALUE",
                    payload: data
                }
            )
        },



    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AnalyticActivity)

