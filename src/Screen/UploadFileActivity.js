import React, { Component, Fragment } from 'react';
import { reader } from '../Component/ExcelReaderComponent'
import { post, get_page } from '../service/service'
import moment from 'moment'
import axios from 'axios'
import tick from "../image/tick.png"
import closeteg from "../image/close.png"
import warning from '../image/warning.png'
import htmlToImage from 'html-to-image';

class UploadFileActivity extends Component {
    constructor() {
        super()
        this.state = {
            file: [],
            data: [],
            code_box: [],
            report_name: null,
            html_page: null,
            uploadPercentage: 0,
            img_upload: "https://image.flaticon.com/icons/svg/2820/2820367.svg",
        }
        // this.iframe_ref = React.createRef();
    }




    upload = async (data, report_name, vending_branch_code, date_start, date_end, index) => {

        let report_data = []

        data.map((data_element, data_index) => {

            if (typeof data_element.actionRefID != "undefined") {
                console.log(data_element.actionRefID)

                report_data.push([null,
                    data_element.actionRefID ? parseInt(data_element.actionRefID) : 0,
                    data_element.created_on,
                    data_element.client_created_on,
                    data_element.vending_branch_code,
                    data_element.dbill_id,
                    data_element.msproduct_id,
                    data_element.product_name,
                    data_element.product_type,
                    data_element.barcode,
                    data_element.code_711,
                    data_element.barcode_711,
                    data_element.normal_price ? parseInt(data_element.normal_price) : 0,
                    data_element.qty ? parseInt(data_element.qty) : 0,
                    data_element.vat ? parseFloat(data_element.vat) : 0,
                    data_element.value ? parseFloat(data_element.value) : 0,
                    data_element.good_price ? parseInt(data_element.good_price) : 0,
                    data_element.vas_price ? parseInt(data_element.vas_price) : 0,
                    data_element.last_price ? parseInt(data_element.last_price) : 0,
                    data_element.payment,
                    data_element.cash_refund ? parseInt(data_element.cash_refund) : 0,
                    data_element.CASH ? parseInt(data_element.CASH) : 0,
                    data_element.CREDITCARD ? parseInt(data_element.CREDITCARD) : 0,
                    data_element.TRUEMONEY ? parseInt(data_element.TRUEMONEY) : 0,
                    data_element.XCASH ? parseInt(data_element.XCASH) : 0,
                    data_element.PROMPTPAY ? parseInt(data_element.PROMPTPAY) : 0,
                    data_element.ALIPAY ? parseInt(data_element.ALIPAY) : 0,
                    data_element.WECHATPAY ? parseInt(data_element.WECHATPAY) : 0,
                    data_element.coin1 ? parseInt(data_element.coin1) : 0,
                    data_element.coin2 ? parseInt(data_element.coin2) : 0,
                    data_element.coin5 ? parseInt(data_element.coin5) : 0,
                    data_element.coin10 ? parseInt(data_element.coin10) : 0,
                    data_element.bank20 ? parseInt(data_element.bank20) : 0,
                    data_element.bank50 ? parseInt(data_element.bank50) : 0,
                    data_element.bank100 ? parseInt(data_element.bank100) : 0,
                    data_element.bank1000 ? parseInt(data_element.bank1000) : 0,
                    data_element.total_cash_in ? parseInt(data_element.total_cash_in) : 0,
                    data_element.total_cash_change ? parseInt(data_element.total_cash_change) : 0,
                    data_element.paymentTSRefID,
                    data_element.paymentTSResult ? data_element.paymentTSResult.replace(/(\r\n|\n|\r)/gm, "") : "",
                    data_element.MID,
                    data_element.TID])
            }
        })

        this.setState({
            data: report_data

        })

        let file_array = this.state.file

        if (report_data.length > 0) {
            let oblect = {
                report_data: report_data,
                report_name: report_name,
                vending_branch_code: vending_branch_code,
                date_start: date_start,
                date_end: date_end
            }


            try {
                await post(oblect, 'report/vending_report/', null).then((res) => {

                    if (res.success) {
                        file_array[index].status = 1
                    } else {
                        if (res.error_code == 50010) {
                            file_array[index].status = -1
                        }
                        // alert(res.error_message)
                    }
                })
            } catch (err) {
                console.log(err)
            }
        } else {
            file_array[index].status = -2
        }


        this.setState({
            file: file_array
        })






        /*Piyapan*/
        // const options = {
        //     onUploadProgress: (ProgressEvent) => {
        //         const { loaded, total } = ProgressEvent;
        //         let percent = Math.floor(loaded + 100 / total)
        //         console.log(`${loaded}kb of ${total}kb | ${percent}%`);
        //     }
        // }

        // axios.post("https://www.mocky.io/v2/5185415ba171ea3a00704eed", data).then(res => {
        //     console.log(res)
        // })
        /*Piyapan*/
    }






    _onUpload = (file) => {
        // let file = this.state.file
        let data = []

        file.map((file_element, index) => {
            if (file_element.status === 0) {
                reader(file_element.file, (res_data) => {

                    if (res_data.length !== 0) {
                        let report_name = file_element.file.name.replace('.xlsx', "")
                        let name_array = report_name.split("__")
                        console.log(name_array)

                        let date_start = name_array[name_array.findIndex((e) => e === "from") + 1]
                        let date_end = name_array[name_array.findIndex((e) => e === "to") + 1]
                        let vending_branch_code = name_array[name_array.findIndex((e) => e === "report_vending") + 1]

                        date_start = moment(date_start, "YYYY_MM_DD_HH_mm_ss").format("YYYY-MM-DD HH:mm:ss")
                        date_end = moment(date_end, "YYYY_MM_DD_HH_mm_ss").format("YYYY-MM-DD HH:mm:ss")

                        let new_report_name = `report_vending__${vending_branch_code}__from__${moment(date_start, "YYYY_MM_DD_HH_mm_ss").format("YYYY_MM_DD_HH_mm_ss")}__to__${moment(date_end, "YYYY_MM_DD_HH_mm_ss").format("YYYY_MM_DD_HH_mm_ss")}`
                        this.upload(res_data, new_report_name, vending_branch_code, date_start, date_end, index)
                    }
                })
            } else {

            }

        })

        console.log("data", data)
    }

    _onProductUpload = async () => {
        let file = this.state.file
        let data = []

        try {
            await reader(file, (res_data) => {
                // this.setState({
                //     data:res_data
                // })
                if (res_data.length !== 0) {

                    // let data = []
                    //     , open_code, msproduct_id, product_name, price, product_picture, product_type, _barcode

                    console.log(res_data)
                    res_data.map((res_data_el) => {
                        if (res_data_el["Master Product ID "]) {
                            data.push([
                                null,
                                res_data_el["Code เบิก"],
                                res_data_el["Master Product ID "],
                                res_data_el.Product_Name,
                                res_data_el.Price,
                                "product/image/" + res_data_el["Master Product ID "] + ".png",
                                res_data_el.Category,
                                "",
                            ])
                        }
                        else {

                        }

                    })






                    let oblect = {
                        data: data
                    }
                    post(oblect, 'product/', null).then((res) => {
                        if (res.success) {
                            alert(res.message)
                        } else {
                            alert(res.error_message)
                        }
                    })
                }
            })
        } catch (err) {
            console.log(err)
        }
        console.log("data", data)

    }




    _onInventoryUpload = async () => {
        let file = this.state.file
        let data = []
        try {
            await reader(file, (res_data) => {
                if (res_data.length !== 0) {
                    console.log(res_data)
                    res_data.map((res_data_el) => {

                        if (res_data_el["Cat"]) {
                            data.push([
                                null,
                                "V0000286A",
                                res_data_el["รหัสวัตถุดิบ / สินค้าสั่ง"],
                                res_data_el["จำนวน"]
                            ])
                        }
                        else {
                        }

                    })
                    let oblect = {
                        data: data
                    }


                    post(oblect, 'inventory/', null).then((res) => {
                        if (res.success) {
                            alert(res.message)
                        } else {
                            alert(res.error_message)
                        }
                    })
                }
            })
        } catch (err) {
            console.log(err)
        }
        console.log("data", data)
    }


    handleFileChange(event) {
        const files = event.target.files;
        console.log("file", files)
        if (files && files[0]) {
            this.setState({
                file: files[0],
                report_name: files[0].name.replace('.xlsx', "")
            });
        }
        // let name_array = name.split("__")
        // console.log("upload_", name_array)

        /*Piyapan*/
        // let data = new FormData();
        // data.append('file', files[0]);

        // const options = {
        //     onUploadProgress: (ProgressEvent) => {
        //         const { loaded, total } = ProgressEvent;
        //         let percent = Math.floor((loaded + 100) / total)
        //         console.log("percent : " + `${loaded}kb of ${total}kb | ${percent}%`);

        //         if (percent < 100) {
        //             this.setState({ uploadPercentage: percent })
        //         }
        //     }
        // }

        // axios.post("https://www.mocky.io/v2/5185415ba171ea3a00704eed", data, options).then(res => {
        //     console.log("res axios: ", res)
        //     this.setState({ avatar: res.data.url, uploadPercentage: 100 }, () => {
        //         // setTimeout(() => {
        //         //     this.setState({ uploadPercentage: 0 })
        //         // }, 5000)
        //     })
        // })
        /*Piyapan*/
    };

    handleMultipleFileChange(event) {
        let files = []
        let files_array = event.target.files;

        // files_array.map((file_el)=>{
        //     customElements.push({
        //         ...file_el,status : 0
        //     })
        // })
        if (files_array) {
            Array.from(files_array).map((files_array_name) => {
                console.log("file", files_array_name.name.split("__")[1])
                let code_name = files_array_name.name.split("__")[1];
                this.setState({ code_box: code_name })
            })
        }
        console.log("name : ", this.state.code_box)
        if (files_array) {
            Array.from(files_array).map((file_el) => {

                console.log(file_el)
                files.push({
                    file: file_el,
                    status: 0
                })
            })
            this.setState({ file: files });
        }




    }

    render_status = (status) => {
        let return_status
        switch (status) {
            case 0: return_status = null
                break;
            case 1: return_status = <img src={tick} style={{ width: 20, height: 20 }} />
                break;
            case -1: return_status = <img src={warning} style={{ width: 20, height: 20 }} />
                break;
            case -2: return_status = <img src={closeteg} style={{ width: 20, height: 20 }} />
                break;
            default:
                break;
        }
        return return_status;
    }

    code_file_code = (code_file_name) => {
        let code_name = code_file_name.split("__")[1];
        return code_name
    }

    render = () => {
        return (
            <div className="bg">
                <div className="row">
                    <div className="col-1"></div>
                    <div className="columns" style={{ marginTop: "100px" }}>
                        <ul className="price">
                            <li class="header">Product Upload</li>
                            <li>
                                <div className="upload-btn-wrapper">
                                    <button className="btn-upload">
                                        <img className="img-upload" src={this.state.img_upload} />
                                        <br />
                                        <label>Upload file</label>
                                    </button>
                                    <input type="file" onChange={(event) => { this.handleFileChange(event) }}
                                        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
                                </div>
                            </li>
                            <li>
                                <button className="btn-default-upload" onClick={() => { this._onProductUpload() }}>
                                    <span>Upload </span>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="columns" style={{ marginTop: "50px" }}>
                        <ul class="price">
                            <li class="header" style={{ backgroundColor: "#4CAF50" }}>Upload Report</li>
                            <li class="grey">
                                <div className="upload-btn-wrapper">
                                    <button className="btn-upload">
                                        <img className="img-upload" src={this.state.img_upload} />
                                        <br />
                                        <label>Upload file</label>
                                    </button>
                                    {/* <p className="fs-14">{this.state.file.name}</p> */}
                                    <input type="file" multiple="multiple" onChange={(event) => { this.handleMultipleFileChange(event) }}
                                        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
                                    {
                                        this.state.file.map((file_element) => {
                                            return (
                                                <table className="fs-16">
                                                    <tr>
                                                        <td style={{width:"130px",height:"30px",padding:0,textAlign:"center"}}>{this.code_file_code(file_element.file.name)}</td>
                                                        <td style={{width:"130px",padding:0,textAlign:"right"}}>{file_element.file.size / 1000} Kb</td>
                                                        <td style={{height:"30px",marginTop:0,padding:9}}>
                                                            {this.render_status(file_element.status)}
                                                        </td>
                                                    </tr>
                                                </table>
                                            )

                                        })
                                    }

                                </div>
                            </li>
                            <li class="grey">
                                <button className="btn-default-upload" onClick={() => { this._onUpload(this.state.file) }}>
                                    <span>Upload </span>
                                </button>
                            </li>

                        </ul>
                    </div>

                    <div class="columns" style={{ marginTop: "100px" }}>
                        <ul class="price">
                            <li class="header">Inventory Upload</li>
                            <li>
                                <div className="upload-btn-wrapper">
                                    <button className="btn-upload">
                                        <img className="img-upload" src={this.state.img_upload} />
                                        <br />
                                        <label>Upload file</label>
                                    </button>
                                    <input type="file" onChange={(event) => { this.handleFileChange(event) }}
                                        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
                                </div>
                            </li>
                            <li >
                                <button className="btn-default-upload" onClick={() => { this._onInventoryUpload() }}>
                                    <span>Upload </span>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div className="col-1"></div>
                </div>
                <div>

                </div>
            </div>
        );
    }
}



export default UploadFileActivity;
