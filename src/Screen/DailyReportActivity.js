import React, { Component, Fragment } from 'react';
import { reader } from '../Component/ExcelReaderComponent'
import { get } from '../service/service'
import moment from 'moment'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import Calader from '../Component/Calader'
import Fade from 'react-reveal/Fade';

import cash from '../image/business.png'
import credit from '../image/credit-card.png'
import truemoney from '../image/truemoney.jpg'
import xcash from '../image/xcash.jpg'
import promptpay from '../image/promptpay.png'
import alipay from '../image/Alipay.png'
import wechat from '../image/wechat.png'
import { connect } from 'react-redux';
import { user_token } from '../Config/constance'
let cash_type = [
    [cash, "CASH"],
    [credit, "CREDITCARD"],
    [truemoney, "TRUEMONEY"],
    [xcash, "XCASH"],
    [promptpay, "PROMPTPAY"],
    [alipay, "ALIPAY"],
    [wechat, "WECHAT"]
]

let tab = [
    "ทั้งหมด",
    "เครื่องดื่ม",
    "ขนม",
    "น้ำอัดลม",
    "ของใช้",
    "ของสด"
]


let scale_color = [
    "FF0000",
    "FF4400",
    "FF6600",
    "FF8800",
    "FFCC00",
    "CCFF00",
    "AAFF00",
    "44FF00",
    "22FF00",
    "00FF00"
]

class DailyReportActivity extends Component {

    constructor() {
        super();
        this.state = {
            activeTab: 'tab1',
            order_data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            sales_data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            order_sum: 0,
            sales_sum: 0,
            payment: [0, 0, 0, 0, 0, 0, 0],
            start_date_report: moment("0000-01-01T00:00:00.000Z"),
            end_date_report: moment("0000-01-01T00:00:00.000Z"),
            date_report: null,
            rating: [],
            rating_product_name: [],
            report_information: [],
            vending_information: [],
            total_price_date: [],
            total_price: [],
            product_pattle: {
                series: [],
                categories: [],
                product_rank: {
                    categories: [],
                    series: []
                },
                product_rank_merge_frequent: {
                    categories: [],
                    series: []
                },
                frequent: {
                    high: [],
                    mid: [],
                    low: []
                }
            },


        }
    }


    make_data = (raw_data, raw_rating_data) => {
        let order_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        let sales_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        let payment = [0, 0, 0, 0, 0, 0, 0]
        let order_sum = 0
        let sales_sum = 0

        let start_date_report = moment()
        let end_date_report = moment("0000-01-01T00:00:00.000Z")
        let rating = []
        let rating_product_name = []




        raw_rating_data.sort((rating_a, rating_b) => {
            let comparison = 0;
            if (rating_a.rating > rating_b.rating) {
                comparison = 1;
            } else if (rating_a.rating > rating_b.rating) {
                comparison = -1;
            }
            return comparison;
        });


        raw_rating_data.map((raw_rating_data_element) => {
            rating.push(raw_rating_data_element.rating)
            rating_product_name.push(raw_rating_data_element.product_name)
        })







        raw_data.map((raw_data_element) => {

            var format = 'HH:mm:ss'
            let i = moment("07:00:00", format).format("HH:mm:ss")
            let index = 0
            let create_time = moment(raw_data_element.created_on).format("HH:mm:ss")


            do {
                let before_time = moment(i, format)
                let after_time = moment(i, format).add(119, 'minutes')

                if (moment(create_time, format).isBetween(before_time, after_time)) {

                    order_data[index] += 1  //count
                    sales_data[index] += raw_data_element.last_price //sum
                }
                i = moment(i, "HH:mm:ss").add(2, 'hours').format("HH:mm:ss")
                index += 1
            } while (i != moment("07:00:00", "HH:mm:ss").format("HH:mm:ss"))


            cash_type.map((cash_type_element, cash_type_index) => {
                if (raw_data_element.payment === cash_type_element[1]) {

                    payment[cash_type_index] += raw_data_element.last_price //sum

                    // console.log(payment)
                }
            })


            if (moment(raw_data_element.created_on).isBefore(start_date_report)) {
                start_date_report = moment(raw_data_element.created_on)
            } else if (moment(raw_data_element.created_on).isAfter(end_date_report)) {
                end_date_report = moment(raw_data_element.created_on)
            }

            // console.log( moment(raw_data_element.created_on).isAfter(end_date_report) )








            order_sum += 1  //count_all
            sales_sum += raw_data_element.last_price //sum_value_all

        })


        this.setState({
            order_data: order_data,
            sales_data: sales_data,
            sales_sum: sales_sum,
            cash_type: cash_type,
            start_date_report: start_date_report,
            end_date_report: end_date_report,
            rating: rating,
            payment: payment,
            rating_product_name: rating_product_name
        })



    }




    get_order = async (report_id, date) => {

        try {
            await get(`report/vending_report/${report_id}`, null).then((res) => {
                if (res.success) {
                    this.make_data(res.result.report, res.result.rating)
                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }



    get_report_information = async (branch_code) => {
        try {
            await get(`report/report_information/${branch_code}`, null).then((res) => {
                if (res.success) {

                    console.log(res)

                    let total_price = []
                    let total_price_date = []


                    // res.result.reverse().map((result_element) => {
                    //     total_price.push(result_element.total_price)
                    //     total_price_date.push(moment(result_element.date_end).format("DD/MM/YYYY"))

                    // })

                    // this.get_pattle_report(branch_code)
                    let report_information = res.result

                    this.setState({
                        report_information: report_information,
                        date_report: moment(report_information[0].date_end).format("DD/MM/YYYY"),
                    })

                    if (res.result[0]) {
                        this.get_order(report_information[0].id)
                    }

                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }

    get_vending_information = async (user_token) => {
        try {
            await get('report/vending_information/', user_token).then((res) => {
                if (res.success) {
                    // this.setState({ vending_information: res.result })

                    // if (res.result[0]) {
                    //     this.get_report_information(res.result[0].vending_branch_code)
                    // }
                    this.get_report_information(res.result[0].vending_branch_code)
                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }

    componentDidMount = () => {

        if (!this.props.VendingList.vending_select_value) {
            this.get_vending_information(user_token)
        } else {
            this.get_report_information(this.props.VendingList.vending_select_value)
        }
    }

    btn_m = (month, index) => {

        // console.log("month", index);
        if (month == "01" || month == "05" || month == "09") {
            if (this.state.date_report == index) {
                return month = "active1";
            }
            return month = "btn-date1";
        }
        if (month == "02" || month == "06" || month == "10") {
            if (this.state.date_report == index) {
                return month = "active2";
            }
            return month = "btn-date2";
        }
        if (month == "03" || month == "07" || month == "11") {
            if (this.state.date_report == index) {
                return month = "active3";
            }
            return month = "btn-date3";
        }
        if (month == "04" || month == "08" || month == "12") {
            if (this.state.date_report == index) {
                return month = "active4";
            }
            return month = "btn-date4";
        }
    }

     copyToClipboard = str => {

        let index = this.props.VendingList.vending_list.findIndex( e => e.vending_branch_code === this.props.VendingList.vending_select_value )
        const el = document.createElement('textarea');
        el.value = this.props.VendingList.vending_list[index].vending_branch_name+" = "+str+" บาท";
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
      };



    render = () => {

        let options = {

            title: {
                text: 'ยอดขาย'
            },
            subtitle: {
                text: `วันที่  ${this.state.start_date_report.format("DD/MM/YYYY")}`
            },
            xAxis: [{
                categories: ['7 - 9 น.', '9 - 11 น.', '11 - 13 น.', '13 - 15 น.', '15 - 17 น.', '17 - 19 น.',
                    '19 - 21 น.', '21 - 23 น.', '23 - 01 น.', '01 - 03 น.', '03 - 05 น.', '05 - 07 น.'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'จำนวนออเดอร์ (ชิ้น)',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'ยอดขาย (บาท)',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || // theme
                    'rgba(255,255,255,0.25)'
            },
            series: [{
                name: 'ยอดขาย',
                type: 'column',
                yAxis: 1,
                data: this.state.sales_data,
                tooltip: {
                    valueSuffix: ' บาท'
                }
            }, {
                name: 'จำนวนออเดอร์',
                type: 'spline',
                data: this.state.order_data,
                tooltip: {
                    valueSuffix: ' ออเดอร์'
                }
            }]
        };


        let rating_options = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'สินค้าขายดี'
            },

            xAxis: {
                categories: this.state.rating_product_name,
                crosshair: true
            },
            yAxis: { // Primary yAxis
                labels: {
                    format: '{value} product_นออเดอร์',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'จำนวนออเดอร์',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            },
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || // theme
                    'rgba(255,255,255,0.25)'
            },
            series: {
                name: 'จำนวนออเดอร์',
                type: 'spline',
                data: this.state.rating,
                tooltip: {
                    valueSuffix: 'ออเดอร์'
                }
            }
        };



        return (
            <div>
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10">
                        <div className="bg-daily" style={{ position: "fixed" }}>
                            <div className="fs-52 center-text animate-top" style={{ color: "#FFF", paddingTop: "15px" }}>Daily Report                            
                            </div>
                            <div className="fs-14 center-text" style={{ color: "#FFF"}}>
                            {this.props.VendingList ? this.props.VendingList.vending_select_value :null}                            
                            </div>
                            <select className="box" value={this.props.VendingList.vending_select_value} onChange={(e) => {
                                this.props.setVendingSelectValue(e.target.value)
                                this.get_report_information(e.target.value)
                            }}>
                                {
                                    this.props.VendingList.vending_list.map((vending_information_element) => {
                                        return (
                                            <option value={vending_information_element.vending_branch_code}>{vending_information_element.vending_branch_code} - {vending_information_element.vending_branch_name}</option>
                                        )
                                    })
                                }

                            </select>
                        </div>
                        <br /><br /><br /><br />
                        

                        
                        {/* <Fragment>
                            <Fade bottom><HighchartsReact highcharts={Highcharts} options={rating_options} /></Fade>
                        </Fragment>


                        <br /><br />
                        <hr />
                        <br /><br /><br /><br /> */}

                        <div style={{ marginTop: "120px" }}>


                            <Fragment>
                                <Fade bottom><HighchartsReact highcharts={Highcharts} options={options} /></Fade>
                            </Fragment>


                            <br /><br />
                            <hr />
                            <br /><br /><br /><br />
                      

                            <Fragment>
                                <Fade bottom>
                                    <div className="div-1024 row">
                                        <div className="col-5 total-list">
                                            <div className="fs-20 total-text"> ยอดขายรวม {this.state.sales_sum} ฿ | {this.state.date_report}</div>
                                            <hr />

                                            {cash_type.map((cash_type_element, cash_type_index) => {
                                                return (
                                                    <table style={{cursor:"pointer"}} onClick={cash_type_index === 0 ? ()=>{this.copyToClipboard(this.state.payment[cash_type_index]) } : ()=>{} } className="report-cash">
                                                        <tr className="report-cashtr">

                                                            <td style={{ width: "50px" }} className="report-cashtd"><img className="img-cash-type" src={cash_type_element[0]} /></td>
                                                            <td style={{ width: "100px" }} className="report-cashtd">{cash_type_element[1]}</td>
                                                            {/* <td style={{ width: "100px" }}>{cash_type_element}</td> */}
                                                            <td style={{ width: "100px", textAlign: "right" }} className="report-cashtd">
                                                                {this.state.payment[cash_type_index]}
                                                            </td>
                                                            <td style={{ width: "10px" }} className="report-cashtd">฿</td>
                                                        </tr>
                                                    </table>
                                                )
                                            })}
                                        </div>
                                        <div className="col-1"></div>
                                        <div className="col-6 total-list">
                                            {this.state.report_information.map((report_information_element, report_information_index) => {
                                                return (
                                                    <button className={this.btn_m(moment(report_information_element.date_end).format("MM"), moment(report_information_element.date_end).format("DD/MM/YYYY"))}
                                                        onClick={() => {
                                                            this.setState({ date_report: moment(report_information_element.date_end).format("DD/MM/YYYY") })
                                                            this.get_order(report_information_element.id)
                                                        }} >
                                                        {/* <td>{report_information_element.vending_branch_code} </td> */}
                                                        {moment(report_information_element.date_end).format("DD/MM/YYYY")}
                                                    </button>
                                                )
                                            })}
                                        </div>
                                    </div>

                                    <div className="div-768 row">
                                        <div className="col-12 total-list">
                                            <div className="fs-20 total-text"> ยอดขายรวม {this.state.sales_sum} ฿ | {this.state.date_report}</div>
                                            <hr />
                                            <div className="row">
                                                <div className="col-9">
                                                    {cash_type.map((cash_type_element, cash_type_index) => {
                                                        return (
                                                            <table className="report-cash">
                                                                <tr className="report-cashtr">
                                                                    <td style={{ width: "50px" }} className="report-cashtd"><img className="img-cash-type" src={cash_type_element[0]} /></td>
                                                                    <td style={{ width: "100px" }} className="report-cashtd">{cash_type_element[1]}</td>
                                                                    {/* <td style={{ width: "100px" }}>{cash_type_element}</td> */}
                                                                    <td style={{ width: "100px", textAlign: "right" }} className="report-cashtd">
                                                                        {this.state.payment[cash_type_index]}
                                                                    </td>
                                                                    <td style={{ width: "10px" }} className="report-cashtd">฿</td>
                                                                </tr>
                                                            </table>
                                                        )
                                                    })}
                                                </div>
                                                {/* <div className="col-1"></div> */}
                                                <div className="col-3 total-768">
                                                    {this.state.report_information.map((report_information_element, report_information_index) => {
                                                        return (
                                                            <button className={this.btn_m(moment(report_information_element.date_end).format("MM"), moment(report_information_element.date_end).format("DD/MM/YYYY"))}
                                                                onClick={() => {
                                                                    this.setState({ date_report: moment(report_information_element.date_end).format("DD/MM/YYYY") })
                                                                    this.get_order(report_information_element.id)
                                                                }} >
                                                                {/* <td>{report_information_element.vending_branch_code} </td> */}
                                                                {moment(report_information_element.date_end).format("DD/MM/YYYY")}
                                                            </button>
                                                        )
                                                    })}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br /><br />
                                </Fade>
                            </Fragment>
                        </div>
                    </div>
                </div>
            </div>
        )
    }




}

const mapStateToProps = (state) => {
    return {
        VendingList: state.VendingList,
    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        setVendingList: (data) => {
            dispatch(
                {
                    type: "SET_VENDING_LIST",
                    payload: data
                }
            )
        },
        setVendingSelectValue: (data) => {
            dispatch(
                {
                    type: "SET_VENDING_SELECT_VALUE",
                    payload: data
                }
            )
        },



    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DailyReportActivity)

