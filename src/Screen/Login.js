import React, { Component } from 'react'
import Logo from '../image/Logo1.jpg';
import { ValidateEmail } from '../Component/EmailValidation'
import { post } from '../service/service'
class Login extends Component {
    constructor(){
        super();
        this.state={
            email:null,
            password:null
        }
    }


    onSubmit = async () => {
        const { email,password } = this.state

        let body = {
            email : email,
            password : password
        }
        if(ValidateEmail(email)){
            try{
                await post(body,"user/user_login").then((res)=>{
                    if(res.success){

                        localStorage.setItem("user_token",res.token)

                        alert(res.message)
                        window.location.href = "/"
                    }else{
                        alert(res.error_message)
                    }
                })

            }catch(err){
                alert(err)
            }
           

            
        }else{
            alert("Email is invalid")
        }
    }



    onInput = (event) => {

        this.setState({
            [event.target.name] : event.target.value
        })
    }






    render() {
        return (
            <div className="row">
                <div className="col-1"></div>
                <div className="col-4">
                    <img src={Logo} className="logo" />
                </div>
                <div className="col-7 bg-yellow">
                    <div className="login_div">
                        <table>
                            <div className="center-text fs-40">Login</div>

                            <tr>
                                <td>Email</td>
                            </tr>
                            <tr>
                                <td><input value={this.state.email} onChange={(e)=>{this.onInput(e)}} className="input-name" name="email" type="email" placeholder="email@address.com" /></td>
                            </tr>
                            <tr>
                                <td>Password</td>
                            </tr>
                            <tr>
                                <td><input  value={this.state.password} onChange={(e)=>{this.onInput(e)}} className="input-name" name="password" type="password" placeholder="Password" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <button onClick={()=>{this.onSubmit()}} className="btn-login">Signin</button>
                                </td>
                            </tr>
                        </table>
                    </div>


                </div>
            </div>
        )
    }
}
export default Login