
import React, { Component } from 'react';
import { reader } from '../Component/ExcelReaderComponent'
import { get } from '../service/service'
import moment from 'moment'
// import { extend } from 'highcharts';
import { Table, Button } from 'evergreen-ui'
import { ip_service } from '../Config/constance'
class ProductActivity extends Component {
    constructor() {
        super();
        this.state = {
            product_list_data: [],
            product_search: "",
            product_list_data_table: []

        }
    }


    _get_product = async () => {
        try {
            await get('product/', null).then((res) => {
                this.setState({ product_list_data: res.result, product_list_data_table: res.result })
            })
        } catch (err) {
            alert(err)
        }

    }




    componentWillMount = () => {
        this._get_product()
    }


    _onSearch = (search_key) => {

        let list_data_arr = []


        this.state.product_list_data.map((product_data) => {
            if (product_data.product_name.toLowerCase().indexOf(search_key) != -1) {
                list_data_arr.push(product_data)
            }
        })



        this.setState({
            product_list_data_table: list_data_arr
        })


    }




    test = (e) => {
        this.setState({ product_search: e })
        console.log(this.state.product_list_data.find(this._onSearch))
    }





    render = () => {



        return (
            <div>
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10">
                        <div className="bg-product" style={{ position: "fixed" }}>
                            <div className="fs-52 center-text animate-top" style={{ color: "#FFF", paddingTop: "15px" }}>Product List</div>
                            <button className="btn-update animate-bottom"><span>UPDATE LIST</span></button>
                        </div>
                    </div>
                    <div className="col-1"></div>
                </div>
                <div style={{ marginTop: "200px" }}>
                    <div className="row">
                        <div className="col-1"></div>
                        <div className="col-10">
                            <Table>
                                <Table.Head>
                                    <Table.TextHeaderCell> โค้ดเบิก </Table.TextHeaderCell>
                                    <Table.TextHeaderCell> รหัสสินค้า </Table.TextHeaderCell>
                                    <Table.TextHeaderCell> รูป </Table.TextHeaderCell>
                                    <Table.SearchHeaderCell onChange={(e) => { this._onSearch(e) }} />
                                    <Table.TextHeaderCell> ราคา </Table.TextHeaderCell>
                                    <Table.TextHeaderCell> barcode </Table.TextHeaderCell>
                                </Table.Head>
                                <Table.Body >
                                    {this.state.product_list_data_table.map(product_element => (
                                        <Table.Row height="60" key={product_element.id} isSelectable onSelect={() => alert(product_element.product_name)}>
                                            <Table.TextCell>{product_element.open_code}</Table.TextCell>
                                            <Table.TextCell>{product_element.msproduct_id}</Table.TextCell>
                                            <Table.TextCell  >
                                                <img style={{ height: 60 }} src={ip_service + product_element.product_picture} />
                                            </Table.TextCell>
                                            <Table.TextCell>{product_element.product_name}</Table.TextCell>
                                            <Table.TextCell>{product_element.price}</Table.TextCell>
                                            <Table.TextCell>{product_element.barcode}</Table.TextCell>
                                        </Table.Row>
                                    ))}
                                </Table.Body>
                            </Table>
                        </div>
                        <div className="col-1"></div>
                    </div>
                </div>
            </div>

        )
    }




}


export default ProductActivity