import React, { Component } from 'react'
import Logo from '../image/Logo1.jpg';
import { ValidateEmail } from '../Component/EmailValidation'
import { post } from '../service/service'
class AddProductActivity extends Component {
    constructor() {
        super();
        this.state = {
            open_code: null,
            msproduct_id: null,
            product_name: null,
            price: null,
            product_picture: null,
            product_type: null,
            barcode: null
        }
    }


    onSubmit = async () => {
        const { open_code, msproduct_id, product_name, price, product_picture, product_type, barcode } = this.state

        let body = {
            open_code: open_code,
            msproduct_id: msproduct_id,
            product_name: product_name,
            price: price,
            product_picture: product_picture.slice(product_picture.indexOf('base64')+1),
            product_type: product_type,
            barcode: barcode
        }

        try {
            await post(body, "product/").then((res) => {
                if (res.success) {
                    alert(res.message)
                    this.setState({
                        open_code: null,
                        msproduct_id: null,
                        product_name: null,
                        price: null,
                        product_picture: null,
                        product_type: null,
                        barcode: null
                    })
                } else {
                    alert(res.error_message)
                }
            })

        } catch (err) {
            alert(err)
        }



    }



    onInput = (event) => {

        this.setState({
            [event.target.name]: event.target.value
        })
    }


    onFileInput = (event) => {

        // var reader = new FileReader();
        // reader.onload() 
        // let img_data = reader.readAsDataURL(event.target.files[0]);


        var file = event.target.files[0]
        var reader = new FileReader();
        var img_data = reader.readAsDataURL(file);

        reader.onloadend = () =>{
           
            this.setState({
                product_picture: reader.result
            })
        }
    
        
    }






    render() {
        return (
            <div className="row">
                <div className="col-7 bg-yellow">
                    <div className="login_div">
                        <table>
                            {/* open_code, msproduct_id, product_name, price, product_picture, product_type, barcode  */}
                            <tr>
                                <td>msproduct_id</td>
                            </tr>
                            <tr>
                                <td><input value={this.state.msproduct_id} onChange={(e) => { this.onInput(e) }} className="input-name" name="msproduct_id" type="text" placeholder="400....." /></td>
                            </tr>
                            <tr>
                                <td>open_code</td>
                            </tr>
                            <tr>
                                <td><input value={this.state.open_code} onChange={(e) => { this.onInput(e) }} className="input-name" name="open_code" type="text" placeholder="400....." /></td>
                            </tr>
                            <tr>
                                <td>product_name</td>
                            </tr>
                            <tr>
                                <td><input value={this.state.product_name} onChange={(e) => { this.onInput(e) }} className="input-name" name="product_name" type="text" placeholder="กาโตะ" /></td>
                            </tr>
                            <tr>
                                <td>price</td>
                            </tr>
                            <tr>
                                <td><input value={this.state.price} onChange={(e) => { this.onInput(e) }} className="input-name" name="price" type="text" placeholder="20" /></td>
                            </tr>
                            <tr>
                                <td>barcode</td>
                            </tr>
                            <tr>
                                <td><input value={this.state.barcode} onChange={(e) => { this.onInput(e) }} className="input-name" name="barcode" type="text" placeholder="88555555555" /></td>
                            </tr>
                            <tr>
                                <td>product_picture</td>
                            </tr>
                            <tr>
                                <td><input onChange={(e) => { this.onFileInput(e) }} className="input-name" type="file" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <button onClick={() => { this.onSubmit() }} className="btn-login">SUBMIT</button>
                                </td>
                                <td>
                                    <img style={{ height: 300, width: 180 }} src={this.state.product_picture} />
                                </td>

                            </tr>


                        </table>
                    </div>


                </div>
            </div>
        )
    }
}
export default AddProductActivity