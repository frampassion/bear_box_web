
import React, { Component } from 'react';
import { reader } from '../Component/ExcelReaderComponent'
import { get, put, post } from '../service/service'
import moment from 'moment'
// import { extend } from 'highcharts';
import { Table, Button, Combobox, Dialog, Tab, Icon } from 'evergreen-ui'
import { ip_service } from '../Config/constance'
import { connect } from 'react-redux'
import { user_token } from '../Config/constance'
// const shelf = require('../Data/esso.json')
import { normalize } from '../Config/normalize'
import htmlToImage from 'html-to-image';
import download from '../image/download.png'
import default_product from '../image/110111009.png'

const merge = [
    { row: 0, col: 1 },
    { row: 0, col: 3 },
    { row: 1, col: 1 },
    { row: 1, col: 3 }
]
class ShelfActivity extends Component {
    constructor() {
        super();
        this.state = {
            product_list_data: [],
            product_search: "",
            product_list_data_table: {
                inventory: [],
                sub_inventory: []
            },
            vending_information: [],
            product_option: [],
            branch_code: '',
            product_value: 0,
            product_obj: null,
            invent_target: "",
            page_name: "stock office",
            page: 1,
            isLoading: false,
            isShown: false,
            from: "",
            to: "",
            product_obj_from: [],
            product_obj_to: [],
            product_obj_invent_arr: [],
            report_information: [],
            row: 0,
            col: 1,
            shelf: [],
            merge_shelf: merge,
            merge_shelf_setting: [],
            json_file: [],
            page_setting: false,
            merge_row: null,
            merge_col: null,
            interval_st: true

        }
    }


    get_shelf = async (branch_code) => {
        try {
            await get(`shelf/shelf_information/${branch_code}`, user_token).then((res) => {
                if (res.success) {


                    this.setState({
                        shelf: res.result.shelf,
                        merge_shelf: res.result.merge
                    })
                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }


    _onSearch = (search_key) => {

        let inventory = []
        let sub_inventory = []

        this.state.product_list_data.inventory.map((product_data) => {
            if (product_data.product_name.toLowerCase().indexOf(search_key) != -1) {
                inventory.push(product_data)
            }
        })

        this.state.product_list_data.sub_inventory.map((product_data) => {
            if (product_data.product_name.toLowerCase().indexOf(search_key) != -1) {
                sub_inventory.push(product_data)
            }
        })




        this.setState({
            product_list_data_table: {
                inventory: inventory,
                sub_inventory: sub_inventory
            }
        })


    }


    sort_arr_obj = (data, obj_name) => {
        data.sort((a, b) => {
            if (a[obj_name] < b[obj_name]) {
                return -1;
            }
            if (b[obj_name] < a[obj_name]) {
                return 1;
            }
            return 0;
        });
        return data
    }





    _get_product_option = async () => {
        try {
            await get('product/', null).then((res) => {

                let product_option = []

                res.result.map((el) => {

                    product_option.push({
                        ...el,
                        label: el.open_code + " - " + el.product_name
                    })
                })

                this.setState({ product_option: product_option })
            })
        } catch (err) {
            alert(err)
        }

    }

    get_vending_information = async (user_token) => {
        try {
            await get('report/vending_information/', user_token).then((res) => {
                if (res.success) {
                    // this.setState({ vending_information: res.result })

                    if (res.result[0]) {
                        this.get_shelf(res.result[0].vending_branch_code)

                        // this.get_report_information(res.result[0].vending_branch_code)
                    }

                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }
    set_shelf = async () => {
        let body = {
            vending_branch_code: this.props.VendingList.vending_select_value,
            vending_shelf: JSON.stringify({
                shelf: this.state.shelf,
                merge: this.state.merge_shelf
            })
        }
        try {
            await put(body, 'shelf/', null).then((res) => {
                if (res.success) {
                    // this.setState({ vending_information: res.result })

                    alert(res.message)

                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }




    componentDidMount = () => {
        // this.get_order()
        this._get_product_option()

        setInterval(() => {
            this.setState({ interval_st: !this.state.interval_st })
        }, 500);


        if (!this.props.VendingList.vending_select_value) {
            this.get_vending_information(user_token)
        } else {
            this.get_shelf(this.props.VendingList.vending_select_value)
        }

        // get_shelf
        let shelf = []


        for (let i = 0; i <= 5; i++) {
            shelf.push([[], [], [], [], [], [], [], [], [], []])

            // for( let j = 1;j <= 5;j++){

            // }

        }
        this.setState({ shelf: shelf })

    }


    changeTable = (path, page_name) => {
        this.setState({ invent_target: path, page_name: page_name })
        setTimeout(() => {
            this._get_product(this.state.branch_code)
        }, 1)
    }

    open_dialog = (from, to) => {
        this.setState({ isShown: true, from: from, to: to })
    }

    close_dialog = (from, to) => {
        this.setState({ isShown: false, product_obj: null })
    }

    add_product_object = () => {
        let shelf = this.state.shelf
        if (this.state.product_obj) {
            shelf[this.state.row][this.state.col] = this.state.product_obj
            this.setState({
                shelf: shelf
            })
            this.close_dialog()
            console.log(this.state.shelf)
        }
    }




    delete_product_object = (index) => {



        let product_obj = this.state.product_obj_invent_arr

        product_obj.splice(index, 1)

        this.setState({
            product_obj_invent_arr: product_obj,
            // product_obj: null,
            // product_value: 0
        })
    }

    shelf_render = () => {

        let row = []
        for (let i = 0; i <= 5; i++) {
            row.push(<div className={"row"}>
                {
                    this.state.page_setting ?
                        this.shelf_col_merg_option_render(i) :
                        this.shelf_col_render(i)}
            </div>)

        }
        return (<div>{row}</div>)
    }

    shelf_check = (i, j) => {
        let st = false


        if (this.state.shelf[i]) {
            if (this.state.shelf[i][j]) {
                st = true
            } else {
                st = false
            }
        } else {
            st = false
        }

        return st
    }




    merge_check = (i, j) => {
        let st = false
        let index = this.state.merge_shelf.findIndex(merge => merge.row == i && merge.col == j)
        if (index >= 0) {
            st = true
        } else {
            st = false
        }
        return st
    }


    check_col_row_merge = (i, j) => {
        let st = false
        let row = this.state.merge_row
        let col = this.state.merge_col

        if (row == i && col <= j + 1 && col >= j - 1 && !this.merge_check(i, j)) {
            st = true
        } else {
            st = false
        }
        return st

    }


    shelf_col_render = (i) => {
        let col = []

        for (let j = 1; j <= 9; j++) {

            // let col_normal = "124px"
            // let col_merge = "273.5px"
            let col_normal = normalize(this.container, 124)
            let col_merge = normalize(this.container, 274)



            if (!this.merge_check(i, j - 1)) {
                col.push(
                    <>
                        {j == 1 ? <hr className="hr-border-l" /> : null}
                        <div className="col-style"
                            style={{
                                width: this.merge_check(i, j) ? col_merge : col_normal,
                                height: normalize(this.container, 140)
                            }}
                            onClick={() => {
                                this.setState({ row: i, col: j, isShown: true })

                                if (this.shelf_check(i, j)) {
                                    if (this.state.shelf[i][j].product_name) {

                                        this.setState({ product_obj: this.state.shelf[i][j] })
                                    }

                                } else {
                                }
                            }}
                        >
                            {
                                this.shelf_check(i, j) ?
                                    this.state.shelf[i][j].product_picture ?
                                        <div style={{ width: "100%" }}>
                                            <div className="row">
                                                <div className="col-4"><div className="number-shelf" style={{ fontSize: normalize(this.container, 10), width: normalize(this.container, 25), height: normalize(this.container, 25) }} >{i}{j}</div></div>
                                                <div className="col-8" style={{ fontSize: normalize(this.container, 12), textAlign: "right", marginTop: "5px" }}>{this.state.shelf[i][j].price} บาท</div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12" >
                                                    <img style={{ height: normalize(this.container, 40), marginLeft: "auto", marginRight: "auto", display: "block" }}
                                                        src={ip_service + this.state.shelf[i][j].product_picture} />
                                                    <div className={!this.merge_check(i, j) ? "center-text name_product" : "center-text"}
                                                        style={{ fontSize: normalize(this.container, 12), width: normalize(this.container, !this.merge_check(i, j) ? 80 : 165) }}
                                                        title={this.state.shelf[i][j].label}>
                                                        {this.state.shelf[i][j].product_name}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        :
                                        <>

                                            <div className="col-4"><div className="number-shelf"
                                                style={{
                                                    fontSize: normalize(this.container, 10),
                                                    width: normalize(this.container, 25),
                                                    height: normalize(this.container, 25)
                                                }} >{i}{j}</div></div>

                                        </>
                                    : null
                            }
                        </div>
                        {/* {i == 5 ? <hr className="hr-border-b" /> : null} */}
                    </>
                )
            } else {

            }
        }

        return (
            <div className="row">
                {col}

            </div>)


    }




    confirm_merge = (i, j) => {

        let row = i
        let col
        let merge_shelf_arr = this.state.merge_shelf

        if (this.state.merge_col < j) {
            col = this.state.merge_col
        } else {
            col = j
        }

        merge_shelf_arr.push(
            {
                row: row,
                col: col
            })


        this.setState({
            merge_col: null,
            merge_row: null,
            merge_shelf: merge_shelf_arr
        })

    }


    un_merge = (i, j) => {

        let row = i
        let col
        let merge_shelf_arr = this.state.merge_shelf

        let index = merge_shelf_arr.findIndex(merg => merg.row === i && merg.col === j)

        merge_shelf_arr.splice(index, 1)

        this.setState({
            merge_shelf: merge_shelf_arr
        })

    }



    shelf_col_merg_option_render = (i) => {
        let col = []

        for (let j = 1; j <= 9; j++) {

            let col_normal = normalize(this.container, 124)
            let col_merge = normalize(this.container, 274)

            //setting
            if (!this.merge_check(i, j - 1)) {
                col.push(
                    <>
                        <div className="col-style"
                            style={{
                                width: this.merge_check(i, j) ? col_merge : col_normal,
                                opacity: this.state.merge_col ? this.check_col_row_merge(i, j) ? 1 : 0.2 : 1,
                                border: "1px solid #000",
                                height: 95
                            }}>
                            {
                                this.shelf_check(i, j) ?
                                    <div>
                                        <div className="col-4"><div className="number-shelf" style={{ fontSize: normalize(this.container, 10), width: normalize(this.container, 25), height: normalize(this.container, 25) }} >{i}{j}</div></div>

                                        <div>
                                            {this.merge_check(i, j) ?
                                                <button className="btn-splid"
                                                    onClick={() => {
                                                        this.un_merge(i, j)
                                                    }}
                                                >Split</button>  // ปุม unmerg แสดงในกรณีเป้นช่องที่ merg แล้ว
                                                :
                                                this.state.merge_col && this.check_col_row_merge(i, j) ?
                                                    <div>
                                                        {
                                                            this.state.merge_col !== j ?
                                                                <button className="btn-merg"
                                                                    onClick={() => {
                                                                        this.confirm_merge(i, j)
                                                                    }} >Merg</button> :   // ปุม merge เมื่อเข้าสู่กระบวนการ merg 
                                                                <button className="btn-merg-cancel"
                                                                    onClick={() => {
                                                                        this.setState({
                                                                            merge_row: null,
                                                                            merge_col: null
                                                                        })
                                                                    }}
                                                                >Cancle</button> // ปุม cancle เมื่อเข้าสู่กระบวนการ merg แล้ว
                                                        }
                                                    </div>
                                                    :
                                                    !this.state.merge_col ?
                                                        <button className="btn-merg"
                                                            onClick={() => {
                                                                this.setState({
                                                                    merge_row: i,
                                                                    merge_col: j
                                                                })
                                                            }} >Merg</button>   // ปุม merge เริ่มต้น
                                                        : null
                                            }
                                        </div>
                                    </div>
                                    : null
                            }
                        </div>
                    </>
                )
            } else {

            }
        }

        return (<div className={"row"} >{col}</div>)


    }



    saveCanvas() {

        // html2canvas(document.getElementById('capture'), {
        //     // allowTaint: true,
        //     // foreignObjectRendering: true
        // }).then((canvas) => {
        //     console.log("canvas", canvas)
        //     var img = canvas.toDataURL("image/png");
        //     const w = window.open('about:blank', 'image from canvas');
        //     w.document.write('<img src="' + img + '"/>');
        // })

        let file_name = `${this.props.VendingList.vending_select_value}__SHELF_${moment().format("YYYY_MM_DD")}.png`


        htmlToImage.toPng(document.getElementById('capture'))
            .then(function (dataUrl) {

                console.log(dataUrl)
                var a = document.createElement("a");
                a.href = dataUrl;
                a.setAttribute("download", file_name);
                a.click();
            });



        // html2canvas(document.getElementById("capture")).then(function (canvas) {
        //     console.log(canvas)

        //     // var canvas = document.getElementById("mycanvas");
        //     var img = canvas.toDataURL("image/png");
        //     const w = window.open('about:blank', 'image from canvas');
        //     w.document.write('<img src="' + img + '"/>');
        // });
    }






    render = () => {
        let inventory_data = []

        if (this.state.page === 1) {
            inventory_data = this.state.product_list_data_table.inventory
        } else if (this.state.page === 2) {
            inventory_data = this.sort_arr_obj(this.state.product_list_data_table.sub_inventory, "value")
        }

        return (
            <div className="row" ref={el => { this.container = el }}>
                <div className="col-1"></div>
                <div className="col-10">
                    <div className="bg-shelf">
                        <div className="fs-52 center-text animate-top" style={{ color: "#FFF", paddingTop: "15px" }}>Planogram</div>

                        <select className="box" value={this.props.VendingList.vending_select_value} onChange={(e) => {
                            this.props.setVendingSelectValue(e.target.value)
                            this.get_shelf(e.target.value)
                        }}>
                            {
                                this.props.VendingList.vending_list.map((vending_information_element) => {
                                    return (
                                        <option value={vending_information_element.vending_branch_code}>{vending_information_element.vending_branch_name}</option>
                                    )
                                })
                            }

                        </select>
                    </div>
                    <div style={{ marginBottom: 0, justifyContent: 'center', marginTop: 100 }} >

                        <div id="capture" style={{ width: "100%", backgroundColor: "#fff" }}  >
                            <button onClick={() => { this.saveCanvas() }} className="downlaod-img">
                                <img src={download} style={{ width: normalize(this.container, 20) }}
                                    className="icon-downlaod-img"
                                />
                                Download image
                            </button>
                            {this.shelf_render()}
                        </div>

                        <hr className="hr-border-b" style={{
                            width: normalize(this.container, 916),
                            opacity: this.state.merge_col ? 0.2 : 1,
                            marginBottom:100
                        }} />
                    </div>
                    <>
                        {
                            this.state.page_setting ?
                                <div className="row" style={{ position: "fixed", bottom: 10, left: 0 }}>
                                    <div className="col-6"><button
                                        className="btn-submit"
                                        onClick={() => { this.setState({ page_setting: false }) }}>
                                        <span>COMFIRM</span>
                                    </button></div>
                                    <div className="col-6"> <button
                                        className="btn-cencle"
                                        onClick={() => { this.setState({ page_setting: false, merge_shelf: JSON.parse(this.state.merge_shelf_setting) }) }} >
                                        <span>CANCLE</span>
                                    </button>
                                    </div>
                                </div>
                                :
                                <div className="row" style={{ position: "fixed", bottom: 10, left: 0 }}>
                                    <div className="col-6"><button className="btn-setting" onClick={() => {
                                        this.setState({ page_setting: true, merge_shelf_setting: JSON.stringify(this.state.merge_shelf) })
                                    }} >
                                        <span>SETTING</span>
                                    </button></div>
                                    <div className="col-6">
                                        <button onClick={() => { this.set_shelf() }} className="btn-save" >
                                            <span>SAVE</span>
                                        </button>
                                    </div>
                                </div>
                        }
                    </>
                </div>
                <div className="col-1"></div>
                <Dialog
                    isShown={this.state.isShown}
                    title="รายการสินค้า"
                    width={600}
                    height="auto"
                    onCloseComplete={() => this.close_dialog()}
                    onConfirm={() => this.add_product_object()}>
                    <div style={{ display: "flex", flexDirection: "column", justifyContent: 'center' }}>

                        {this.state.product_obj ?
                            <div className="row" style={{ marginBottom: 15 }}>
                                <div className="col-2"></div>
                                <div className="col-2">
                                    <img style={{ height: 70, marginLeft: "auto", marginRight: "auto", display: "block" }}
                                        src={ip_service + this.state.product_obj.product_picture} />
                                </div>
                                <div className="col-6">
                                    <div>หมายเลขช่องสินค้า : {this.state.row}{this.state.col}</div>
                                    <div>{this.state.product_obj.product_name}</div>
                                    ราคา : {this.state.product_obj.price} บาท
                                </div>
                                <div className="col-2"></div>
                            </div>
                            :
                            <div className="row" style={{ marginBottom: 15 }}>
                                <div className="col-2"></div>
                                <div className="col-2">
                                    <img style={{ height: 70, marginLeft: "auto", marginRight: "auto", display: "block" }}
                                        src={default_product} />
                                </div>
                                <div className="col-6">
                                    <div>หมายเลขช่องสินค้า : {this.state.row}{this.state.col}</div>
                                    <div>ชื่อสินค้า</div>
                                    ราคา : 0 บาท
                            </div>
                                <div className="col-2"></div>
                            </div>

                        }

                        <div className="row">
                            <div className="col-12 center-text">
                                <div>
                                    <Combobox
                                        items={this.state.product_option}
                                        width={300}
                                        openOnFocus
                                        selectedItem={this.state.product_obj}
                                        itemToString={item => item ? item.label : ''}
                                        onChange={selected => this.setState({ product_obj: selected })}
                                        placeholder="สินค้า"
                                        autocompleteProps={{
                                            // Used for the title in the autocomplete.
                                            title: 'สินค้า'
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </Dialog>
            </div>
        )
    }



}

const mapStateToProps = (state) => {
    return {
        VendingList: state.VendingList,
    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        setVendingList: (data) => {
            dispatch(
                {
                    type: "SET_VENDING_LIST",
                    payload: data
                }
            )
        },
        setVendingSelectValue: (data) => {
            dispatch(
                {
                    type: "SET_VENDING_SELECT_VALUE",
                    payload: data
                }
            )
        },



    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShelfActivity)

