import React, { Component } from 'react'
import NotFound_img from '../image/NotFound.png'
import { user_token } from '../Config/constance'
import { Redirect } from 'react-router-dom'
class NotFound extends Component {

    render() {
        if (!user_token) {
            return (<Redirect to="/" />)
        }
        return (
            <div style={{ backgroundColor: "#ffc933" }}>
                <img src={NotFound_img} style={{ width: "100%", height: "99.4vh" }} />
            </div>
        )
    }
}
export default NotFound;