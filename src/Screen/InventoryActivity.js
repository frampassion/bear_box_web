
import React, { Component } from 'react';
import { reader } from '../Component/ExcelReaderComponent'
import { get, put, post } from '../service/service'
import moment from 'moment'
// import { extend } from 'highcharts';
import { Table, Button, Combobox, Dialog, Tab, Icon } from 'evergreen-ui'
import { ip_service } from '../Config/constance'
import { connect } from 'react-redux'
import { user_token } from '../Config/constance'
import {
    ExportToCSV
} from "../Component/ExportToCSV";
import "../Style/Inven.css"
import dl from "../image/dl.png"

class InventoryActivity extends Component {
    constructor() {
        super();
        this.state = {
            product_list_data: [],
            product_search: "",
            product_list_data_table: {
                inventory: [],
                sub_inventory: []
            },
            vending_information: [],
            product_option: [],
            branch_code: '',
            product_value: 0,
            product_obj: null,
            invent_target: "",
            page_name: "stock office",
            page: 1,
            isLoading: false,
            isShown: false,
            from: "",
            to: "",
            product_obj_from: [],
            product_obj_to: [],
            product_obj_invent_arr: [],
            report_information: [],
            report_id: null,
            file: null,
            date_e: ""

        }
    }


    _get_product = async (id) => {
        this.get_report_information(id)
        try {
            await get(`inventory/${id}`, null).then((res) => {


                console.log(res.result)

                this.setState({ product_list_data_table: res.result, product_list_data: res.result })

                this.setState({ branch_code: id })
            })
        } catch (err) {
            alert(err)
        }

    }

    _get_sell_product = async (id, date_id) => {
        try {
            await get(`inventory/sell_product/${id}`, null).then((res) => {


                if (res.success) {
                    this.setState({ product_obj_invent_arr: res.result, report_id: id })
                } else {
                    alert(res.err_message)
                }
                // this.setState({ product_obj_invent_arr: res.result })
            })
        } catch (err) {
            alert(err)
        }
        this.setState({ date_e: date_id })
    }

    _get_recomend_fill_product = async () => {
        let id = this.props.VendingList.vending_select_value
        try {
            await get(`inventory/recommend/${id}`, null).then((res) => {
                if (res.success) {
                    this.setState({ product_obj_invent_arr: res.result })
                } else {
                    alert(res.err_message)
                }
                // this.setState({ product_obj_invent_arr: res.result })
            })
        } catch (err) {
            alert(err)
        }

    }

    _insert_recomend_fill_product = async () => {

        let body = {
            vending_branch_code: this.props.VendingList.vending_select_value,
            product_recomend: JSON.stringify(this.state.product_obj_invent_arr)
        }

        try {
            await post(body, `inventory/recommend`, null).then((res) => {
                if (res.success) {
                    alert(res.message)
                } else {
                    alert(res.err_message)
                }
                this.setState({
                    isShown: false,
                    product_obj: null,
                    product_value: 0
                })
            })
        } catch (err) {
            alert(err)
        }

    }





    _update_inventory = async () => {

        let product_obj = this.state.product_obj_invent_arr





        this.setState({ isLoading: true })


        if (product_obj) {


            let product_obj_from = []
            let product_obj_to = []
            product_obj.map((product_obj_el) => {
                product_obj_from.push([null, product_obj_el.branch_code, product_obj_el.open_code, product_obj_el.product_value * -1])
                product_obj_to.push([null, product_obj_el.branch_code, product_obj_el.open_code, product_obj_el.product_value])
            })








            let body = {
                product_from: product_obj_from,
                product_to: product_obj_to,
                from: this.state.from,
                to: this.state.to,
                report_id: this.state.report_id || null
            }

            console.log(body)




            // let body = [null,obj.product_obj.branch_code,obj.product_obj.open_code.]
            try {
                await put(body, `inventory/`, null).then((res) => {
                    // alert(res.message)

                    this.setState({ isLoading: false })
                    this.setState({ isShown: false })

                    setTimeout(() => {
                        this._get_product(this.state.branch_code)


                    }, 100)


                    // this.setState({ product_list_data: res.result, product_list_data_table: res.result })
                })
            } catch (err) {
                alert(err)
            }

        }
    }
    get_report_information = async (branch_code) => {
        try {
            await get(`report/report_information/${branch_code}`, null).then((res) => {
                if (res.success) {


                    this.setState({
                        report_information: res.result,
                    })

                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }





    // componentWillMount = () => {
    //     this._get_product()
    // }


    _onSearch = (search_key) => {

        let inventory = []
        let sub_inventory = []

        this.state.product_list_data.inventory.map((product_data) => {
            if (product_data.product_name.toLowerCase().indexOf(search_key) != -1) {
                inventory.push(product_data)
            }
        })

        this.state.product_list_data.sub_inventory.map((product_data) => {
            if (product_data.product_name.toLowerCase().indexOf(search_key) != -1) {
                sub_inventory.push(product_data)
            }
        })




        this.setState({
            product_list_data_table: {
                inventory: inventory,
                sub_inventory: sub_inventory
            }
        })


    }


    sort_arr_obj = (data, obj_name) => {
        data.sort((a, b) => {
            if (a[obj_name] < b[obj_name]) {
                return -1;
            }
            if (b[obj_name] < a[obj_name]) {
                return 1;
            }
            return 0;
        });
        return data
    }




    test = (e) => {
        this.setState({ product_search: e })
        console.log(this.state.product_list_data.find(this._onSearch))
    }

    _get_product_option = async () => {
        try {
            await get('product/', null).then((res) => {

                let product_option = []

                res.result.map((el) => {

                    product_option.push({
                        ...el,
                        label: el.open_code + " - " + el.product_name
                    })
                })

                this.setState({ product_option: product_option })
            })
        } catch (err) {
            alert(err)
        }

    }

    get_vending_information = async (user_token) => {
        try {
            await get('report/vending_information/', user_token).then((res) => {
                if (res.success) {
                    // this.setState({ vending_information: res.result })

                    if (res.result[0]) {
                        this._get_product(res.result[0].vending_branch_code)

                        // this.get_report_information(res.result[0].vending_branch_code)
                    }

                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }




    componentDidMount = () => {
        // this.get_order()
        this._get_product_option()



        if (!this.props.VendingList.vending_select_value) {
            this.get_vending_information(user_token)
        } else {
            this._get_product(this.props.VendingList.vending_select_value)
        }
    }

    // product_list_option = (product_list) => {
    //     let list = []
    //     product_list.map((el) => {
    //         list.push(el.open_code + " - " + el.product_name)
    //     })

    //     console.log(product_list)

    //     return list
    // }

    changeTable = (path, page_name) => {
        this.setState({ invent_target: path, page_name: page_name })
        setTimeout(() => {
            this._get_product(this.state.branch_code)
        }, 1)
    }

    open_dialog = (from, to) => {
        this.setState({ isShown: true, from: from, to: to })
    }

    add_product_object = () => {



        let product_obj = this.state.product_obj_invent_arr

        if (this.state.product_value) {
            product_obj.push({
                branch_code: this.state.branch_code,
                open_code: this.state.product_obj.open_code,
                product_name: this.state.product_obj.product_name,
                product_picture: this.state.product_obj.product_picture,
                product_value: this.state.product_value
            })

            this.setState({
                product_obj_invent_arr: product_obj,
                product_obj: null,
                product_value: 0
            })
        }
    }

    add_product_object_from_value = (open_code, product_name, product_picture, product_value) => {



        let product_obj = this.state.product_obj_invent_arr


        product_obj.push({
            branch_code: this.state.branch_code,
            open_code: open_code,
            product_name: product_name,
            product_picture: product_picture,
            product_value: product_value
        })

        this.setState({
            product_obj_invent_arr: product_obj,
            product_obj: null,
            product_value: 0
        })
    }

    delete_product_object = (index) => {



        let product_obj = this.state.product_obj_invent_arr

        product_obj.splice(index, 1)

        this.setState({
            product_obj_invent_arr: product_obj,
            // product_obj: null,
            // product_value: 0
        })
    }


    onInputshelfInformation = (file) => {

        if (file) {
            reader(file, (res_data) => {
                // res_data
                let sub_inventory = this.state.product_list_data_table.sub_inventory
                let unique_res_data = []

                res_data.map((res_el) => {

                    let res_el_index = unique_res_data.findIndex(e => e.Id == res_el.Id)
                    console.log(res_el_index)
                    console.log(res_el)
                    if (res_el_index >= 0) {
                        unique_res_data[res_el_index].Max += res_el.Max
                    } else {
                        unique_res_data.push(res_el)
                    }




                })


                unique_res_data.map((res_data_el) => {
                    let inventory_index = sub_inventory.findIndex(e => e.msproduct_id == res_data_el.Id)
                    console.log(inventory_index)
                    if (inventory_index >= 0 && res_data_el.Id != "420700004" && res_data_el.Max != "1") {
                        sub_inventory[inventory_index].Max = res_data_el.Max
                        sub_inventory[inventory_index].total = sub_inventory[inventory_index].value - res_data_el.Max
                    } else {

                    }
                })

                this.setState({
                    product_list_data_table: {
                        ...this.state.product_list_data_table,
                        sub_inventory: this.sort_arr_obj(sub_inventory, "total")
                    }
                })

            })
        }


    }

    onRecomend = () => {


        this._get_recomend_fill_product().then(() => {


            if (this.state.product_obj_invent_arr.length <= 0) {
                let arr = this.state.product_list_data_table.sub_inventory

                console.log(arr)
                arr.filter(e => e.total < 6).map((arr_el) => {
                    this.add_product_object_from_value(arr_el.open_code, arr_el.product_name, arr_el.product_picture, 6 - arr_el.total)
                })
            }

        })




        this.open_dialog("pre", "sub_inventory")
    }

    handleFileChange(event) {
        const files = event.target.files;
        console.log("file", files)
        if (files && files[0]) this.setState({ file: files[0] });
    }

    excel_download = () => {

        let dataUrl = this.state.product_obj_invent_arr


        let CSVdata = ""

        CSVdata += "ลำดับที่,รายการ                           ,จำนวน" + `\r\n`
        dataUrl.map((dataUrl_element, index) => {



            CSVdata += index + 1 + "," + dataUrl_element.product_name + "," + dataUrl_element.product_value + `\r\n`



        })





        setTimeout(() => {
            ExportToCSV("รายการนำของลงตู้ " + this.render_vending_name(), CSVdata)
        }, 100)
    }





    check_total = async () => {


        try {
            await get(`inventory/total_recommend/12`, null).then((res) => {
                if (res.success) {

                    let dataUrl = this.sort_arr_obj(res.result,'open_code')


                    let CSVdata = ""

                    CSVdata += "ลำดับที่,รายการ                           ,จำนวน" + `\r\n`
                    dataUrl.map((dataUrl_element, index) => {



                        CSVdata += index + 1 + "," + dataUrl_element.product_name + "," + dataUrl_element.product_value + `\r\n`



                    })





                    setTimeout(() => {
                        ExportToCSV("รายการนำของลงตู้ " + this.render_vending_name(), CSVdata)
                    }, 100)

                } else {
                    console.log("ERROR !!! :", res.message)
                }
            }).catch((err) => {
                console.log(err)
            })
        } catch (err) {
            console.log(err)
        }
    }

    render_vending_name = () => {
        let index = this.props.VendingList.vending_list.findIndex(e => e.vending_branch_code === this.props.VendingList.vending_select_value)
        return this.props.VendingList.vending_list[index].vending_branch_name
    }

    btn_m = (month, index) => {

        // console.log("month", index);
        if (month == "01" || month == "05" || month == "09") {
            if (this.state.date_report == index) {
                return month = "active159";
            }
            return month = "btn-date159";
        }
        if (month == "02" || month == "06" || month == "10") {
            if (this.state.date_report == index) {
                return month = "active2610";
            }
            return month = "btn-date2610";
        }
        if (month == "03" || month == "07" || month == "11") {
            if (this.state.date_report == index) {
                return month = "active3711";
            }
            return month = "btn-date3711";
        }
        if (month == "04" || month == "08" || month == "12") {
            if (this.state.date_report == index) {
                return month = "active4812";
            }
            return month = "btn-date4812";
        }
    }



    render = () => {
        let inventory_data = []

        if (this.state.page === 1) {
            inventory_data = this.state.product_list_data_table.inventory
        } else if (this.state.page === 2) {
            inventory_data = this.sort_arr_obj(this.state.product_list_data_table.sub_inventory, "total")
        }

        return (
            <div>
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10">
                        <div className="bg-inventory" style={{ position: "fixed" }}>
                            <div className="fs-52 center-text animate-top" style={{ color: "#FFF", paddingTop: "15px" }}>Inventory</div>

                            <select className="box" value={this.props.VendingList.vending_select_value} onChange={(e) => {
                                this.props.setVendingSelectValue(e.target.value)
                                this._get_product(e.target.value)
                            }}>
                                {
                                    this.props.VendingList.vending_list.map((vending_information_element) => {
                                        return (
                                            <option value={vending_information_element.vending_branch_code}>{vending_information_element.vending_branch_name}</option>
                                        )
                                    })
                                }

                            </select>
                        </div>

                        <div style={{ marginTop: "200px" }}>
                            <div className="row" >
                                <div className="col-4"></div>
                                <div className="col-4">
                                    <Button onClick={() => { 
                                        this._get_recomend_fill_product()
                                        this.open_dialog("inventory", "sub_inventory") 
                                        }} appearance="primary">บ้าน {"=>"}  ตู้</Button>
                                    <Button onClick={() => { this.open_dialog("sub_inventory", "inventory") }} appearance="primary">บ้าน {"<="} ตู้</Button>
                                    <Button onClick={() => { this.open_dialog("", "inventory") }} appearance="primary">CP มาส่งของ</Button>
                                    <Button onClick={() => { this.open_dialog("sub_inventory", "customer") }} appearance="primary">ขาย ตัดจ่าย</Button>
                                </div>
                                <div className="col-4"></div>
                            </div>


                            <Tab
                                onClick={() => { this.setState({ page: 1 }) }}
                                isSelected={this.state.page === 1}
                                height={50}
                                width={200}

                            >บ้าน</Tab>
                            <Tab
                                onClick={() => { this.setState({ page: 2 }) }}
                                isSelected={this.state.page === 2}
                                height={50}
                                width={200}
                                style={{
                                    backgroundColor: "#ff00000"
                                }}
                            >ตู้</Tab>

                            <div style={{ fontSize: 30 }}> {this.state.page === 1 ? "สต๊อคใหญ่" : this.state.page === 2 ? "สต๊อคข้างตู้" : null}</div>

                            อัพโหลดไฟล์ ข้อมูลหน้าตู้  <input type="file" onChange={(event) => { this.handleFileChange(event) }} />
                            <button onClick={() => { this.onInputshelfInformation(this.state.file) }}> Generate </button>

                            <button onClick={() => { this.onRecomend() }}> Recomend </button>

                            {/* <button onClick={() => { this.onRecomend() }}> Caribate Stock </button> */}
                            <div style={{ marginBottom: 200 }} >

                                <Table>
                                    <Table.Head>
                                        <Table.TextHeaderCell> โค้ดเบิก </Table.TextHeaderCell>
                                        {/* <Table.TextHeaderCell> รหัสสินค้า </Table.TextHeaderCell> */}
                                        <Table.TextHeaderCell> รุป </Table.TextHeaderCell>
                                        <Table.SearchHeaderCell onChange={(e) => { this._onSearch(e) }} />
                                        <Table.TextHeaderCell> จำนวน ทั้งหมด </Table.TextHeaderCell>
                                        {this.state.page != 1 ? <Table.TextHeaderCell> จำนวน หน้าตู้ </Table.TextHeaderCell> : null}
                                        {this.state.page != 1 ? <Table.TextHeaderCell> จำนวน ข้างตู้ </Table.TextHeaderCell> : null}

                                    </Table.Head>
                                    <Table.Body >
                                        {inventory_data.map(product_element => (
                                            <Table.Row height="60" key={product_element.id} isSelectable onSelect={() => alert(product_element.product_name)}>
                                                <Table.TextCell>{product_element.open_code}</Table.TextCell>
                                                {/* <Table.TextCell>{product_element.msproduct_id}</Table.TextCell> */}
                                                <Table.TextCell  >
                                                    <img style={{ height: 60 }} src={ip_service + product_element.product_picture} />
                                                </Table.TextCell>
                                                <Table.TextCell>{product_element.product_name}</Table.TextCell>
                                                <Table.TextCell>{product_element.value}</Table.TextCell>
                                                {this.state.page != 1 ? <Table.TextCell>{product_element.Max ? product_element.Max : "-"}</Table.TextCell> : null}
                                                {this.state.page != 1 ? <Table.TextCell>{product_element.total ? product_element.total : ' - '}</Table.TextCell> : null}

                                            </Table.Row>
                                        ))}
                                    </Table.Body>
                                </Table>

                                <Dialog
                                    isShown={this.state.isShown}
                                    title="รายการสินค้า"
                                    width={600}
                                    height="auto"
                                    onCloseComplete={() => this.setState({ isShown: false, isLoading: false, from: "from", to: "", product_obj_invent_arr: [], product_value: 0 })}
                                    isConfirmLoading={this.state.isLoading}
                                    onConfirm={() => this.state.from == "pre" ? this._insert_recomend_fill_product() : this._update_inventory()}
                                    confirmLabel={this.state.isLoading ? 'Loading...' : 'Confirm'}>
                                    {/* Dialog content */}
                                    <div id="top" />
                                    <div style={{ display: "flex", flexDirection: "row" }}>

                                        <Combobox
                                            items={this.state.product_option}
                                            width={340}
                                            openOnFocus
                                            selectedItem={this.state.product_obj}
                                            // items={[{ label: 'Banana' }, { label: 'Pear' }, { label: 'Kiwi' }]}
                                            itemToString={item => item ? item.label : ''}
                                            onChange={selected => this.setState({ product_obj: selected })}
                                            placeholder="สินค้า"
                                            autocompleteProps={{
                                                // Used for the title in the autocomplete.
                                                title: 'สินค้า'
                                            }}
                                        />


                                        {/* <div style={{ fontSize: 20 }}> จำนวน </div> */}
                                        <input type="number" style={{ width: 100, height: 28, fontSize: 16, marginRight: 15 }} onChange={(e) => { this.setState({ product_value: e.target.value }) }} value={this.state.product_value} />

                                        <Icon style={{ marginRight: 15, cursor: "pointer" }} onClick={() => { this.add_product_object() }} icon="add" color="info" size={35} />


                                        {/* <Button onClick={() => { this.add_product_object() }} appearance="primary">   <Icon icon="add" color size={40}/> เพิ่ม</Button> */}

                                    </div>




                                    <>{this.state.date_e}</>
                                    <Table>
                                        <Table.Head>
                                            <Table.TextHeaderCell> โค้ดเบิก </Table.TextHeaderCell>
                                            <Table.TextHeaderCell> รุป </Table.TextHeaderCell>
                                            <Table.TextHeaderCell> ชื่อ </Table.TextHeaderCell>
                                            <Table.TextHeaderCell> จำนวน </Table.TextHeaderCell>
                                            <Table.TextHeaderCell>  </Table.TextHeaderCell>

                                        </Table.Head>
                                        <Table.Body >
                                            {this.state.product_obj_invent_arr.map((product_element, index) => (
                                                <Table.Row height="60" key={product_element.id}  >
                                                    <Table.TextCell>{product_element.open_code}</Table.TextCell>
                                                    <Table.TextCell  >
                                                        <img style={{ height: 50 ,width: 30}} src={ip_service + product_element.product_picture} />
                                                    </Table.TextCell>
                                                    <Table.TextCell> {product_element.product_name}</Table.TextCell>
                                                    <Table.TextCell>

                                                        <input type="number" style={{ width: 50, height: 28, fontSize: 16, marginRight: 15 }} onChange={(e) => {
                                                            let arr = this.state.product_obj_invent_arr
                                                            arr[index].product_value = e.target.value
                                                            this.setState({ product_value: arr })

                                                        }} value={product_element.product_value} />

                                                    </Table.TextCell>
                                                    <Table.TextCell><Icon style={{ marginRight: 15, cursor: "pointer" }} onClick={() => { this.delete_product_object(index) }} icon="delete" color="danger" size={20} /></Table.TextCell>

                                                </Table.Row>
                                            ))}
                                        </Table.Body>
                                    </Table>



                                    <button onClick={() => { this.excel_download() }}
                                        className="btn-ex-dl"
                                    ><img src={dl} className="img-excel-dl"/> Excel
                                    </button>
                                    <button onClick={() => { this.check_total() }}>excel Download total</button>

                                    <div style={{ display: "flex", flexDirection: "column" }}>
                                        {this.state.report_information.filter(e => e.cut_stock == 0).map((report_information_element, report_information_index) => {

                                            return (
                                                <a href="#top" style={{ textDecoration: "none", color: "#000" }}>
                                                    <button onClick={() => { this._get_sell_product(report_information_element.id, moment(report_information_element.date_end).format("DD/MM/YYYY")) }}
                                                        className={
                                                            this.btn_m(moment(report_information_element.date_end).format("MM"),
                                                                moment(report_information_element.date_end).format("DD/MM/YYYY"))}
                                                    >

                                                        {report_information_element.vending_branch_code} | 
                                                        วันที่ {moment(report_information_element.date_strat).format("DD/MM/YYYY HH:mm")} - 
                                                        {moment(report_information_element.date_end).format("DD/MM/YYYY HH:mm")}
                                                    </button>
                                                </a>

                                            )
                                        })}
                                    </div>

                                </Dialog>
                            </div>
                        </div>
                    </div>
                    <div className="col-1"></div>
                </div>


            </div>
        )
    }




}

const mapStateToProps = (state) => {
    return {
        VendingList: state.VendingList,
    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        setVendingList: (data) => {
            dispatch(
                {
                    type: "SET_VENDING_LIST",
                    payload: data
                }
            )
        },
        setVendingSelectValue: (data) => {
            dispatch(
                {
                    type: "SET_VENDING_SELECT_VALUE",
                    payload: data
                }
            )
        },



    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InventoryActivity)

