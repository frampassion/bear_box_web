import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';
import { BrowserRouter as Router, Link, Switch, Route } from 'react-router-dom'
import { get } from "./service/service"
import upload from '../src/image/up-arrow.png'
import report from '../src/image/report.png'
import productlist from '../src/image/checklist.png'
import inventory from '../src/image/supplier.png'
import logout from '../src/image/logout.png'
import sum from '../src/image/tools-and-utensils.png'
import { connect } from 'react-redux';
import shelf from '../src/image/signs.png'
import MainRoute from './Router/MainRoute'
import LoginRoute from './Router/LoginRoute'

import { user_token } from './Config/constance'

class App extends Component {
  constructor() {
    super();
    this.state = {
      vending_information: [],
      vending_option_value: null,
      username: "null",
      password: "null",
      status_btn: false,
    }
  }



  get_vending_information = async (token) => {
    try {
      await get('report/vending_information?id=1001', token).then((res) => {
        if (res.success) {
          // this.setState({ vending_information: res.result })
          this.props.setVendingList(res.result)

          if (res.result[0]) {
            // this.setState({ vending_option_value: res.result[0].vending_branch_code })
            // this.props.setVendingList(res.result)
            // this.get_report_information(res.result[0].vending_branch_code)
            this.props.setVendingSelectValue(res.result[0].vending_branch_code)
          }
        } else {
          console.log("ERROR !!! :", res.message)
        }
      }).catch((err) => {
        console.log(err)
      })
    } catch (err) {
      console.log(err)
    }
  }

  setOption = (event) => {
    this.setState({ vending_option_value: event })
  }

  componentWillMount = () => {
    if (user_token) {
      this.get_vending_information(user_token)
    }
  }
  render = () => {
    const userLink = (
      <div>
        <div className="div-1024">
          <div id="mySidenav" className="sidenav">
            <Link to={{ pathname: "/upload", params: null }} id="UPLOAD-FILE">
              <div className="fs-16"> UPLOAD FILE <img src={upload} className="icon-sidenav" /> </div>
            </Link>
            <Link to={{ pathname: "/report", params: null }} id="REPORT">
              <div className="fs-16"> REPORT <img src={report} className="icon-sidenav" /> </div>
            </Link>
            <Link to={{ pathname: "/product", params: null }} id="PRODUCT-LIST">
              <div className="fs-16"> PRODUCT LIST <img src={productlist} className="icon-sidenav" /> </div>
            </Link>
            <Link to={{ pathname: "/inventory", params: null }} id="INVENTORY">
              <div className="fs-16"> INVENTORY <img src={inventory} className="icon-sidenav" /> </div>
            </Link>
            <Link to={{ pathname: "/daily_report", params: null }} id="DAILY">
              <div className="fs-16"> DAILY REPORT <img src={sum} className="icon-sidenav" /> </div>
            </Link>
            <Link to={{ pathname: "/shelf", params: null }} id="SHELF">
              <div className="fs-16">PLANOGRAM <img src={shelf} className="icon-sidenav" /> </div>
            </Link>
            <Link to={{ pathname: "/", params: null }} id="LOGOUT">
              <div onClick={() => { localStorage.setItem("user_token", null) }} className="fs-16"> LOGOUT <img src={logout} className="icon-sidenav" /> </div>
            </Link> 
          </div>
        </div>


        <div className="div-768 mobile-container">
          <button className="btn-status" style={{ float: "right" }} onClick={() => { this.setState({ status_btn: !this.state.status_btn }) }}>
            {this.state.status_btn == true ? <span>:::</span> : <div style={{ color: "#fff" }}>MANU</div>}
          </button>
          {this.state.status_btn == true ?
            <div className="topnav">
              <ul>
                <li><div className="topnav-link">
                  <div className="fs-16" onClick={() => { this.setState({ status_btn: false }) }} style={{ cursor: "pointer", float: "right" }}> X </div>
                </div></li>
                <li><Link to={{ pathname: "/upload", params: null }} className="topnav-link">
                  <div className="fs-16" onClick={() => { this.setState({ status_btn: !this.state.status_btn }) }}> UPLOAD FILE <img src={upload} className="icon-sidenav" /> </div>
                </Link></li>
                <li><Link to={{ pathname: "/report", params: null }} className="topnav-link">
                  <div className="fs-16" onClick={() => { this.setState({ status_btn: !this.state.status_btn }) }}> REPORT <img src={report} className="icon-sidenav" /> </div>
                </Link></li>
                <li><Link to={{ pathname: "/product", params: null }} className="topnav-link">
                  <div className="fs-16" onClick={() => { this.setState({ status_btn: !this.state.status_btn }) }}> PRODUCT LIST <img src={productlist} className="icon-sidenav" /> </div>
                </Link></li>
                <li><Link to={{ pathname: "/inventory", params: null }} className="topnav-link">
                  <div className="fs-16" onClick={() => { this.setState({ status_btn: !this.state.status_btn }) }}> INVENTORY <img src={inventory} className="icon-sidenav" /> </div>
                </Link></li>
                <li><Link to={{ pathname: "/daily_report", params: null }} className="topnav-link">
                  <div className="fs-16" onClick={() => { this.setState({ status_btn: !this.state.status_btn }) }}> DAILY REPORT <img src={sum} className="icon-sidenav" /> </div>
                </Link></li>
                <li><Link to={{ pathname: "/shelf", params: null }} className="topnav-link">
                  <div className="fs-16" onClick={() => { this.setState({ status_btn: !this.state.status_btn }) }}> SHELF <img src={shelf} className="icon-sidenav" /> </div>
                </Link></li>
                <li><Link to={{ pathname: "/", params: null }} className="topnav-link">
                  <div className="fs-16" onClick={() => { this.setState({ status_btn: !this.state.status_btn }) }}> LOGOUT <img src={logout} className="icon-sidenav" /> </div>
                </Link></li>
              </ul>
            </div>
            :
            null
          }
        </div>
      </div>
    )


    return (
      <Router >
        {user_token != null ? userLink : null}

        {
          user_token ?
            <MainRoute />
            : <LoginRoute />
        }






      </Router>
    );
  }
}



const mapStateToProps = (state) => {
  return {
    VendingList: state.VendingList,
  }

}
const mapDispatchToProps = (dispatch) => {
  return {
    setVendingList: (data) => {
      dispatch(
        {
          type: "SET_VENDING_LIST",
          payload: data
        }
      )
    },
    setVendingSelectValue: (data) => {
      dispatch(
        {
          type: "SET_VENDING_SELECT_VALUE",
          payload: data
        }
      )
    },



  }
}





export default connect(mapStateToProps, mapDispatchToProps)(App)



