import React, { Component } from 'react';

//    export const ip = "http://192.168.0.107:3017/api/v1/"

import { ip_service } from '../Config/constance'
export const ip = ip_service





export const post = (object, path, token) => new Promise((resolve, reject) => {
    fetch(ip + path, {
        method: 'POST',
        headers: {
            'Authorization': token,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }, body: JSON.stringify(object)
    }).then(res => {


        setTimeout(() => null, 0);
        return res.json()
    }).then(json => {
        resolve(json);
    }).catch((err) => reject(err))

})


export const put = (object, path, token) => new Promise((resolve, reject) => {
    fetch(ip + path, {
        method: 'PUT',
        headers: {
            'Authorization': token,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }, body: JSON.stringify(object)
    }).then(res => {


        setTimeout(() => null, 0);
        return res.json()
    }).then(json => {
        resolve(json);
    }).catch((err) => reject(err))

})




export const get = (path, token) => new Promise((resolve, reject) => {
    fetch(ip + path, {
        method: 'GET',
        headers: {
            'Authorization': token,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(res => {

        setTimeout(() => null, 0);
        return res.json()
    }).then(json => {
        resolve(json);
    }).catch((err) => reject(err))

})








export const get_other = (path, token) => new Promise((resolve, reject) => {
    fetch(path, {
        method: 'GET',
        mode: 'no-cors'
    }).then(res => {
        setTimeout(() => null, 0);
        return res.blob()
    }).then(blob => {
        console.log(blob)
        resolve(blob);
    }).catch((err) => reject(err))

})


export const get_page = (path, token) => new Promise((resolve, reject) => {
    fetch(path, {
        method: 'GET',
        header: {
            'Content-Type': 'text/html; charset=UTF-8',
            'Accept': 'text/html; charset=UTF-8',
            // 'Content-Type': 'application/json'
        },

        mode: 'no-cors'
    }).then(res => {
        setTimeout(() => null, 0);
        // return res.blob()
        // console.log(res)
        // var parser = new DOMParser();
        // var html = parser.parseFromString(res, 'text/html');
        //  console.log("res",res.json())

        return res
    }).then(html => {
        console.log(html)
        resolve(html);
    }).catch((err) => reject(err))
    // fetch(path, {
    //     method: 'GET',
    //     headers: {
    //         'Authorization': token,
    //         'Accept': 'application/json',
    //         'Content-Type': 'application/json'
    //     }
    // }).then(res => {

    //     setTimeout(() => null, 0);
    //     return res.json()
    // }).then(json => {
    //     resolve(json);
    // }).catch((err) => reject(err))

})


