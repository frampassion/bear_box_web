import React, { Component } from "react";
import { Switch, Route } from "react-router-dom"
import UploadFileActivity from '../Screen/UploadFileActivity'
import NotFound from '../Screen/NotFound';
import Login from '../Screen/Login';

class LoginRoute extends Component {

    render() {
        return (


            <Switch>
                <Route exact path="/" component={Login} />
                <Route path="/Login" component={Login} />

                <Route component={NotFound} />
            </Switch>



        );
    }
}

export default LoginRoute