import React, { Component } from "react";
import { Switch  ,Route} from "react-router-dom"
import UploadFileActivity from '../Screen/UploadFileActivity'
import AnalyticActivity from '../Screen/AnalyticActivity'
import ProductActivity from '../Screen/ProductActivity'
import InventoryActivity from '../Screen/InventoryActivity'
import DailyReportActivity from '../Screen/DailyReportActivity'
import ShelfActivity from '../Screen/ShelfActivity';
import NotFound from '../Screen/NotFound'
import AddProductActivity from '../Screen/AddProductActivity'

class MainRoute extends Component {

    render() {
        return (


            <Switch>
                <Route exact path="/" component={UploadFileActivity} />
                <Route path="/upload" component={UploadFileActivity} />
                <Route path="/report" component={AnalyticActivity} />
                <Route path="/product" component={ProductActivity} />
                <Route path="/inventory" component={InventoryActivity} />
                <Route path="/daily_report" component={DailyReportActivity} />
                <Route path="/shelf" component={ShelfActivity} />
                <Route path="/addproduct" component={AddProductActivity} />
                <Route component={NotFound} />
            </Switch>



        );
    }
}


export default MainRoute