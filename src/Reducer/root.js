import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';


import VendingListReducer from './VendingListReducer';



const rootReducer = combineReducers({
    VendingList: VendingListReducer,

    router: routerReducer,
  

   

});

export default rootReducer;
