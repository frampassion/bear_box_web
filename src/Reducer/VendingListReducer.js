const initialState = {
    vending_list : [],
    vending_select_value : null
}

const VendingListReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_VENDING_LIST":
            state = {
                ...state,
                vending_list: action.payload,
            }
            break;
            case "SET_VENDING_SELECT_VALUE":
            state={
                ...state,
                vending_select_value:action.payload
            }
            break;
        default:
            break;
    }
    return state;
}




export default VendingListReducer;