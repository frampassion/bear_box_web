import React, { Component } from 'react';
import ReactLightCalendar from '@lls/react-light-calendar'
import '@lls/react-light-calendar/dist/index.css'
import moment from 'moment'

// const DAY_LABELS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
// const MONTH_LABELS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
class Calader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            startDate: new Date().getTime(), // Today
            endDate: new Date(new Date().getTime()).setDate(new Date().getDate() - 6) // Today + 6 days
        }
    }

    onChange = (startDate, endDate) => {
        // console.log("startDate",moment(startDate).format('DD-MM-YYYY'))
        // console.log("endDate",moment(endDate).format('DD-MM-YYYY'))
        this.setState({ startDate, endDate })

        if(startDate && endDate ){
            if(startDate>endDate){
                this.props.get_pattern_data(endDate,startDate)
            }else{
                this.props.get_pattern_data(startDate,endDate)
            }
           
        }

    }

    render = () => {

        return (
            <div className="center-text">
                {moment(this.state.startDate).format('DD/MM/YYYY') >= moment(this.state.endDate).format('DD/MM/YYYY') ?
                    <div> {moment(this.state.endDate).format('DD/MM/YYYY')} - {moment(this.state.startDate).format('DD/MM/YYYY')}</div>
                    :
                    <div> {moment(this.state.startDate).format('DD/MM/YYYY')} ถึง {moment(this.state.endDate).format('DD/MM/YYYY')}</div>
                }
                <ReactLightCalendar startDate={this.state.startDate} endDate={this.state.endDate} onChange={this.onChange} range />

            </div>

        )
    }
}

export default Calader;