import XLSX from 'xlsx';

export const reader = (file,setdata) => {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    let data = []

    reader.onload = (e) => {
        /* Parse data */
        const bstr = e.target.result;
        const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array', bookVBA: true });
        /* Get first worksheet */
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];
        /* Convert array of arrays */
        data = XLSX.utils.sheet_to_json(ws);

        // console.log(data)
        setdata(data)
        // return ['1']

    };


    if (file) {
        console.log("if")
        if (rABS) {
            reader.readAsBinaryString(file);
        } else {
            reader.readAsArrayBuffer(file);
        };    
    } else {
        console.log("else")
        data = []
    }




    /* Return Data */


}
