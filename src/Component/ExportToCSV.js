import moment from 'moment'

export const ExportToCSV = (name,csv_data) => {
    var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=UTF-8 ,\uFEFF' + csv_data
        hiddenElement.target = '_blank';
        hiddenElement.download = `${name}_${moment().format('YYYY-MM-DD_hh:mm:ss')}.csv`;
        hiddenElement.click();
    return 0;
 }